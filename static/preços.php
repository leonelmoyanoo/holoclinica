<?php $render = true; $titulo='Preços'; $customHeader = $titulo;require_once('../includes/header.php'); $scripts = ['precos','actividades']; ?>

<?php $bg='#B4B6B6';include('../includes/descripcion.php'); ?>

<div class="text-center my-3">
	<h5>Saiba mais! Conheca todas as <a href="#postsHolder">nossas atividades.</a></h5>
	<div class="container my-3">
		<div class="row">
			<div class="card text-white bg-primary  col-md-4">
			  <div class="card-header">Atividade Avulsa </div>
			  <div class="card-body">
			    <h5 class="card-title">16h as 18h</h5>
			    <h5 class="card-title"> R$ 65</h5>
			  </div>
			</div>
			<div class="card text-white bg-primary  col-md-4">
			  <div class="card-header">Plano Mensal de 5 Atividades Semanais </div>
			  <div class="card-body">
			    <h5 class="card-title">16h as 18h</h5>
			    <h5 class="card-title">R$ 692</h5>
			  </div>
			</div>
			<div class="card text-white bg-primary  col-md-4">
			  <div class="card-header">Plano Mensal de 1 Atividade Semanal </div>
			  <div class="card-body">
			    <h5 class="card-title">16h as 18h</h5>
			    <h5 class="card-title">R$ 180</h5>
			  </div>
			</div>
		</div>
	</div>
		
	
<hr>
	<div class="container my-3">
		<div class="row">
			<div class="card text-white bg-secondary  col-md-6">
			  <div class="card-header">Periodo Avulso </div>
			  <div class="card-body">
			    <h5 class="card-title">13h as 18h</h5>
			    <h5 class="card-title">R$ 90</h5>
			  </div>
			</div>
			<div class="card text-white bg-secondary  col-md-6">
			  <div class="card-header">Plano Mensal Período</div>
			  <div class="card-body">
			    <h5 class="card-title">13h as 18h</h5>
			    <h5 class="card-title">R$ 750</h5>
			  </div>
			</div>
		</div>
	</div>
</article>
		
	<hr>
</div>


	<div class="container bg-light" id="postsHolder" style="display: none;">
	  <div class="display-3 text-center mb-3">Conheça nossas atividades</div>

      <div class="row">
	  		<!-- Second section -->
	  </div>
	</div>
<?php  require_once('../includes/footer.php');?>
