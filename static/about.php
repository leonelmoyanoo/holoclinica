<?php $render = true; $titulo='Quem somos?'; $customHeader = $titulo; require_once('../includes/header.php'); $scripts = []; ?>

	<div class="container bg-light mt-3 mb-3 p-4 aboutHolder"style="display: block;">
		<h4>
			A Casa do Conhecimento é um co-studying, em que as crianças desenvolvem o interesse e o prazer de estudar fazendo suas tarefas escolares no seu tempo e escolhem a matéria do seu interesse para se aprofundar e compartilhar, sempre assistidas pela equipe de monitores. <br>
			<br>
			Além disso temos OFICINAS de música, artes, culinária, yoga, meditação, artes cênicas, expressão corporal, inglês, espanhol, em horários determinados e conduzidas por oficineiros especialistas. <br>
			<br>
			Este ambiente multietário e com diversas possibilidades de escolha amplia o conjunto de descobertas, desenvolvendo as crianças de maneira integral. <br>
			<br>
			Na Casa do Conhecimento não tem matrícula nem inscrição: é só vir e estudar quando quiser!  <br>
			<br>
			A Casa do Conhecimento é um espaço cheio de atividades ludicas que estimulam e desenvolvem as crianças. <br>
			<br>
			Uma alternativa para os pais que não tem com quem deixar seus filhos fora do horario escolar, ou como atividade complementar para quem quer estudar de maneira inteligente no contraturno. Oferecemos musicalização infantil, artes, contação de histórias, cultura popular, culinária e muitas brincadeiras e oficinas. Atendemos por hora ou período, sem dia fixo, conforme a necessidade de cada família.  <br>
			<br>
			E nos finais de semana, a nossa Casa é palco de deliciosas festas. Perfeitas para quem quer fugir do modelo tradicional dos buffets infantis, mas procura um espaço feito para crianças com muito charme e diversão garantida. <br>
		</h4>
	</div>
<!-- Card -->
<div class="card testimonial-card">

  <!-- Background color -->
  <div class="card-up indigo lighten-1"></div>

  <!-- Avatar -->
  <div class="avatar mx-auto white">
    <img src="../assets/images/about/Carla Cristina Fonseca Soares.jpeg" class="rounded-circle"
      alt="woman avatar">
  </div>

  <!-- Content -->
  <div class="card-body">
    <!-- Name -->
    <h3 class="card-title">Carla Cristina Fonseca Soares - Educadora e Terapeuta Vibracional </h3>
    <hr>


    <!-- Quotation -->
    <h4><i class="fas fa-caret-right"></i> 
    	Pós-Graduação Latu Sensu em Teorias e Técnicas para Cuidados Integrativos pela UNIFESP Universidade Federal de São Paulo, 2019.
    </h4>
    <h4><i class="fas fa-caret-right"></i> 
    	Praticante de Barras de Acces pela Facilitadora Bianca Navarro, Acces Consciousness®, São Paulo, SP - 2019. 
    </h4>
    <h4><i class="fas fa-caret-right"></i> 
		Capacitação em Sistema Arcturus pela Escola Flor da Vida, São Paulo, SP - 2018. 
    </h4>
    <h4><i class="fas fa-caret-right"></i> 
		Capacitação em Reiki 1 e 2 pela Reiki Master Denise Ap. S. de Andrade, São Paulo, SP - 2018.
    </h4>
    <h4><i class="fas fa-caret-right"></i> 
		Pós-Graduação Latu Sensu em Acupuntura pela ETOSP- Escola de Terapias Orientais de São Paulo, SP – 2009.
    </h4>
    <h4><i class="fas fa-caret-right"></i> 
		Capacitação em Terapia Floral pela Revista Brasileira de Terapia Floral, São Paulo, SP - 2007.
    </h4>
    <h4><i class="fas fa-caret-right"></i> 
		Qualificação Profissional em Artes Cênicas pela Casa das Artes de Laranjeiras – CAL (DRT: 056033), Rio de Janeiro, RJ - 2003.
    </h4>
    <h4><i class="fas fa-caret-right"></i> 
		Youth Leader, Gymnastics and Sports Instructor Education pela Gymnastikhojskolen i Ollerup, Ollerup, Dinamarca 1998.

    </h4>
    <h4><i class="fas fa-caret-right"></i> 
		Licenciatura em Educação Física pela Faculdade de Educação Física da UNICAMP, Campinas, SP – 1996.
    </h4>
  </div>

</div>
<!-- Card -->

<!-- Card -->
<div class="card testimonial-card">

  <!-- Background color -->
  <div class="card-up indigo lighten-1"></div>

  <!-- Avatar -->
  <div class="avatar mx-auto white">
    <img src="../assets/images/about/Miguel Grisi Nettos.jpeg" class="rounded-circle"
      alt="woman avatar">
  </div>

  <!-- Content -->
  <div class="card-body">
    <!-- Name -->
    <h3 class="card-title">Miguel Grisi Nettos</h3>
    <hr>
    <!-- Quotation -->
    <h4><i class="fas fa-caret-right"></i> 
		Formação em Farmacia Bioquímica.
    </h4>
    <h4><i class="fas fa-caret-right"></i> 
    	Formação em Programação Neurolinguística.
    </h4>
    <h4><i class="fas fa-caret-right"></i> 
		Experiência em gestão de pessoas.
    </h4>
    <h4><i class="fas fa-caret-right"></i> 
    	Professor particular de inglês.
    </h4>  
    <h4><i class="fas fa-caret-right"></i> 
		Desenvolvimento e liderança - DL - INEXH - Instituto Nacional em Excelência Humana.
    </h4>
    <h4><i class="fas fa-caret-right"></i> 
    	A Consciência da Vida - Pina Medeiros.
    </h4>
    <h4><i class="fas fa-caret-right"></i> 
    	SpeedTech - Edutech – SEBRAE.
    </h4>
</div>

</div>

<?php  require_once('../includes/footer.php');?>
