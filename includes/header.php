<?php
    if (isset($render)) {
        $puntos=file_exists('styles/custom.css')?'':'../';
        session_start();
        require_once($puntos.'config.php');
    } else {
        exit();
    }
    //VARIABLE QUE SE AGREGA ANTES DE CADA REDIRECCION POR SI EL ARCHIVO SE ENCUENTRA EN OTRA CARPETA QUE NO SEA LA RAIZ

?>

<html>
    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="<?=$puntos?>vendor/twbs/bootstrap/dist/css/bootstrap.min.css" crossorigin="anonymous">


        <!-- Our custom setup -->
        <title><?= $info['area_name'] ?> - <?= $info['company_name'] ?></title>
        <link type="text/css" rel="stylesheet" href="<?=$puntos?>styles/custom.css">
        <link rel="stylesheet" href="<?=$puntos?>styles/custom.css" crossorigin="anonymous">
        <script src="https://kit.fontawesome.com/d37d07f1ad.js" crossorigin="anonymous"></script>

        <!-- Trumbowyg loaded from CDN. TODO: Find a way to fetch it off composer. -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/Trumbowyg/2.20.0/ui/trumbowyg.min.css" integrity="sha256-B6yHPOeGR8Rklb92mcZU698ZT4LZUw/hTpD/U87aBPc=" crossorigin="anonymous" />

        <!-- Bootstrap 4 Toast jQuery plugin from GitHub. TODO: Find a way to fetch it off composer. -->
        <link rel="stylesheet" type="text/css" href="<?=$puntos?>thirdparty/Script47/Toast/styles/toast.min.css" crossorigin="anonymous" />
    </head>
    <body>
        <?php 
            if (isset($inicio) || isset($customHeader) || isset($justBg)) {
                $url = 'assets/images/background.jpg';
                $style = 'background-size: cover;';
            }else if (isset($img)) {
                $url = 'assets/images/'.$img;
            }else{
                $url='';
            }

        ?>
        <?php if (isset($style)): ?>
            <header style="background: url('<?=$puntos?><?=$url?>');<?=$style?>">
        <?php else: ?>
            <header>
        <?php endif ?>
            <nav class="navbar navbar-expand-lg navbar-light bg-transparent pt-5">

                <a class="navbar-brand text-white" href="<?= $puntos ?>index.php">
                    <img class="img-logo ml-4" src="<?=$puntos?>assets/images/logo.png" alt="Casa do conhecimento" onclick="displayBusyIndicator()" />
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse text-center" 
                    id="navbarSupportedContent">
                    <ul class="navbar-nav ml-auto pr-4">
                        <li class="nav-item bg-light mr-2 rounded font-weight-bold active">
                            <a class="nav-link" href="<?= $puntos ?>TV.php">TV</a>
                        </li>
                        <li class="nav-item bg-light mr-2 rounded font-weight-bold active">
                            <a class="nav-link" href="<?= $puntos ?>blog.php">Blog</a>
                        </li>
                        <li class="nav-item bg-light mr-2 rounded font-weight-bold">
                            <a class="nav-link" href="<?= $puntos ?>Biblioteca.php">Biblioteca</a>
                        </li>
                        <li class="nav-item bg-light mr-2 rounded font-weight-bold">
                            <a class="nav-link" href="<?= $puntos ?>static/about.php">Quem somos</a>
                        </li>
                        <li class="nav-item bg-light mr-2 rounded font-weight-bold">
                            <a class="nav-link" href="<?= $puntos ?>Contato.php">Contato</span></a>
                        </li>
                    </ul>
                    <div class="pl-4 text-center">
                        <button class="facebook btn btn-outline-light my-2 my-sm-0" type="submit">Facebook</button>
                        <button class="btn btn-outline-light my-2 my-sm-0" type="submit">Instagram</button>
                    </div>
                </div>
            </nav>
            <nav class="navbar navbar-expand-lg navbar-light bg-transparent">
                <div class="collapse navbar-collapse text ml-3 text-center" id="navbarSupportedContent">
                    <ul class="navbar-nav">
                        <li class="nav-item">
                            <a class="nav-link text-white font-weight-bold" href="<?=$puntos?>PROGRAMAÇÃO.php">Programação</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link text-white font-weight-bold" href="<?=$puntos?>Festa no Quintal.php">Festa no Quintal</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link text-white font-weight-bold" href="<?=$puntos?>Educomunicação.php">Educomunicação</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link text-white font-weight-bold" href="<?=$puntos?>Ferias.php">Férias</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link text-white font-weight-bold" href="<?=$puntos?>static/preços.php">Preços</a>
                        </li>
                    </ul>
                </div>
            </nav>
          <?php
          if (isset($inicio)) {
            print '
              <div class="jumbotron p-4 p-md-5 text-white rounded bg-transparent py-6">
                <div class="col-md-5 px-0">
                  <h1 class="display-4 font-italic">Fazer lição de casa nunca foi tão divertido</h1>
                  <p class="lead my-3">Oficina de apresentador de TV para jovens de 7 a 14 anos.</p>
                  <p class="lead mb-0"><button class="text-black font-weight-bold btn btn-outline-light btn btn-lg btn-block btn-outline-primary" id="Asista-agora">Asista agora!</button></p>
                </div>
              </div>';
          } elseif (isset($customHeader)) {
            print '
              <div class="jumbotron p-4 p-md-5 text-white rounded bg-transparent py-6">
                <div class="col-md-12 px-0">
                  <h1 id="articleTitle" class="display-4 font-italic text-center">' . ($customHeader === true ? 'Carregando...' : $customHeader) . '<h1>
                  <div id="articleKeywords" class="text-center w-100"></div>
                </div>
              </div>';
          }
          ?>
        </header>
        <?php if (!isset($style)): ?>
            <?php if (file_exists($url)): ?>
                <img src="<?=$puntos?><?=$url?>" style="width: 100%;">
            <?php endif ?>
        <?php endif ?>
        <main>
            <?php
                if (isset($error)) {
                    print $error;

                    require_once($puntos.'<?=$puntos?>includes/footer.php');
                    exit();
                }
            ?>
