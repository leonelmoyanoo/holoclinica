<?php
    if (!isset($render)) {
        exit();
      }
    ?>
                <?php 
                  $texto="Olá! Para receber os conteúdos da Casa do Conhecimento no WhatsApp você deve enviar esta mensagem e salvar este número na sua agenda. Não se esqueça de salvar este número na sua agenda, sem isso não temos como enviar os conteúdos!"; 
                  $texto = str_replace(" ","%20",$texto);
                  $wsp = "https://wa.me/5511972816677";
                  $wsp.="?text=";
                  $wsp.=$texto; 
                ?>
    </main>
  </body>
    <!-- Footer -->
    <footer class="page-footer font-small blue pt-4 footer-op"  style="width: 100%;">
      <!-- Grid column -->
          
      <hr class="clearfix w-100 d-md-none">
      
      <!-- Footer Links -->
      <div class="text-center text-md-left"   style="width: 100%;">

        <!-- Grid row -->
        <div class="row">

          <!-- Grid column -->
          <div class="col-md-3 mx-auto">

            <!-- Links -->
            <div class="row">
              <div class="col-2">
                <img class="img-footer" src="<?=$puntos?>assets/images/footer/contacts.png" />
              </div>
              <div class="col">
                <h5 class="heading-footer font-weight-bold text-uppercase mt-3 mb-4">contatos</h5>
              </div>
            </div>            

            <ul class="list-unstyled">
              <li>
                <i class="fas fa-caret-right"></i>
                
                <a href="<?=$wsp?>" target="_blank">
                  +55 11 97281-6677
                </a>
              </li>
              <li>
                <i class="fas fa-caret-right"></i>
                <a href="mailto: comunica@ebrasil.art.br" target="_blank">
                  comunica@ebrasil.art.br
                </a>
              </li>
              <li>
                <i class="fas fa-caret-right"></i>
                <a href="https://www.instagram.com/estacaobrasil/" target="_blank">
                  Instagram
                </a>
              </li>
              <li>
                <i class="fas fa-caret-right"></i>
                <a href='https://www.facebook.com/CasadoConhecimentoSP' target="_blank">
                  Facebook
                </a>
              </li>
              <li>
                <i class="fas fa-caret-right"></i>
                <a href="https://www.youtube.com/channel/UC2kitZZvWY7WiWhYAWnHqGw" target="_blank">
                  Youtube
                </a>
              </li>
            </ul>

          </div>
          <!-- Grid column -->


          <!-- Grid column -->
          <div class="col-md-3 mx-auto">

            <!-- Links -->
            <div class="row">
              <div class="col-2">
                <img class="img-footer" src="<?=$puntos?>assets/images/footer/location.png" />
              </div>
              <div class="col">
                <h5 class="heading-footer font-weight-bold text-uppercase mt-3 mb-4">endereço</h5>
              </div>
            </div>         

            <ul class="list-unstyled">
              <li>
                <i class="fas fa-caret-right"></i>
                <a href="https://www.google.com/maps/place/Esta%C3%A7%C3%A3o+Brasil+%7C+Casa+de+Arte+e+Cultura/@-23.6285843,-46.5566831,15z/data=!4m5!3m4!1s0x0:0x67a44311dc153a99!8m2!3d-23.6285843!4d-46.5566831" target="_blank">Avenida Presidente Kennedy, 1895 <br>
                  Bairro Santa Paula<br>
                  São Caetano do Sul - SP<br>
                  Aberto todos os dias das 8h às 22h
                </a>
              </li>
            </ul>

          </div>
          <!-- Grid column -->


          <!-- Grid column -->
          <div class="col-md-3 mx-auto text-center text-black card border-success" id="footer-direccion">
                <p class="card-text">Faça parte da comuidade da Casa do Conhecimento e receba todas as informações pelo WhatsApp. Clique no botão e siga as orientações.</p>
                <div class="card-footer">
                  <div id="wsp-box" class="text-center card bg-success ">
                    <a href="<?=$wsp?>"  target="_blank" class="text-white">Cadastrar</a>
                  </div>
                </div>
          </div>
          <!-- Grid column -->


      </div>
      <!-- Footer Links -->

      <!-- Copyright -->
      <div class="footer-copyright text-center py-3" id="copyright"  style="width: 100%;">
            © 2019 eBrasil. Todos os direitos reservados. Customizado por: 
            <a>Agência 13vinte</a>
            <img src="<?=$puntos?>assets/images/footer/13vinte.jpg" class="img-fluid" alt="Responsive image">
            <br>
      </div>
      <!-- Copyright -->


    <!-- jQuery -->
    <script src="<?=$puntos?>vendor/components/jquery/jquery.min.js" crossorigin="anonymous"></script>

    <!-- Our custom common script. -->
    <script src="<?=$puntos?>scripts/common.js"></script>

    <!-- Bundled Bootstrap JS and Chart.JS. -->
    <script src="<?=$puntos?>vendor/twbs/bootstrap/dist/js/bootstrap.bundle.min.js" crossorigin="anonymous"></script>
    <script src="<?=$puntos?>vendor/nnnick/chartjs/dist/Chart.min.js" crossorigin="anonymous"></script>

    <!-- Trumbowyg loaded from CDN. TODO: Find a way to fetch it off composer. -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Trumbowyg/2.20.0/trumbowyg.min.js" integrity="sha256-oFd4Jr73mXNrGLxprpchHuhdcfcO+jCXc2kCzMTyh6A=" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Trumbowyg/2.20.0/langs/pt_br.min.js" integrity="sha256-WE4JDJAT6W+jfqTzziDDSgDE0rtufto1ouJeldP/xj0=" crossorigin="anonymous"></script>    

    <?php 
        //SCRIPTS 
        if (isset($scripts) && is_array($scripts)) {
          foreach ($scripts as $script) {
            print '<script defer src="'.$puntos.'scripts/' . $script . '.js"></script>';
          }
        }
    ?>

 <!--   <i id="showBottomBar" class="fa fa-chevron-up text-white my-auto bg-warning"></i>
    <div id="mail-news-bar" class="fixed-bottom d-flex justify-content-center py-1 bg-warning">
        <div class="my-auto d-block position-absolute col-12 text-right pr-4 h-100">
          <i id="hideBottomBar" class="fa fa-chevron-down text-white my-auto bg-warning"></i>
        </div>
        <div class="row col-12 col-lg-10">
          <div class="d-block d-lg-none text-center text-white col-12 my-auto pb-1">Receba nossas novidades por email!</div>
          <div class="d-none d-lg-block text-right text-white col-lg-5 my-auto">Receba nossas novidades por email!</div>
          <input type="email" class="form-control col-9 col-lg-4" id="emailNewsText" aria-describedby="emailNews" placeholder="Seu melhor email :)">
          <button class="btn btn-outline-light ml-2">Assine</button>
        </div>
    </div>
-->
    </footer>
    <!-- Footer -->

    <!-- Bootstrap 4 Toast jQuery plugin from GitHub. TODO: Find a way to fetch it off composer. -->
    <script src="<?=$puntos?>thirdparty/Script47/Toast/scripts/toast.min.js"></script>

    <?php
      print '
    <script>
      const TARGET_THUMBNAIL_PATH = \'' . TARGET_THUMBNAIL_PATH . '\';
      const ANIMATION_TIME        = ' . ANIMATION_TIME . '; // ms
    </script>';
    ?>

    <div id="globalBusyIndicator">
      <div class="spinner-border text-warning" role="status">
        <span class="sr-only">Loading...</span>
      </div>
    </div>
</html>