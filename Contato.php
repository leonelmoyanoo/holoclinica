<?php $render = true;$titulo='Casa do Conhecimento'; $customHeader = $titulo;;require_once('includes/header.php'); $scripts = [ '' ]; ?>

<article>
	<div class="text-center">
		<div class="container-header rounded-sm">
			<h4 class="text-center display-4 text-black ">
			    Localização
			</h4>
		</div>
		<iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d14621.293343913378!2d-46.5566789!3d-23.6285889!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x67a44311dc153a99!2sEsta%C3%A7%C3%A3o%20Brasil%20%7C%20Casa%20de%20Arte%20e%20Cultura!5e0!3m2!1ses-419!2sar!4v1581797229042!5m2!1ses-419!2sar" width="600" height="450" frameborder="0" style="border:0;" allowfullscreen=""></iframe>
		<hr>
		<div class="container-header rounded-sm">
			<h4 class="text-center display-4 text-black ">
			    Horario de Funcionamento:
			</h4>
		</div>
		<div class="container-body  text-center">
			<div class="container bg-light mt-3 mb-3 p-4 aboutHolder" style="display: block;">
			    <h4 class="text-dark text-center mb-0">
			    	Da 2a a 6a, das 13h às 18h
			    </h4>
			</div>
		</div>
			    <hr>




		<div class="container-header rounded-sm">
			<h4 class="text-center display-4 text-black ">
			    Telefone e Whatsapp
			</h4>
		</div>
		<div class="container-body  text-center">
			<div class="container bg-light mt-3 mb-3 p-4 aboutHolder" style="display: block;">
			    <h4 class="text-dark text-center mb-0">
			    	Carla:<a href="https://wa.me/5511972816677 "  target="_blank"> +55 (11) 97281-6677</a>
			    </h4>
			    <h4 class="text-dark text-center mb-0">
			    	Miguel:<a href="https://wa.me/5511947344448"  target="_blank"> +55 (11) 94734-4448</a>
			    </h4>
			</div>
		</div>
			    <hr>



		<div class="container-header rounded-sm">
			<h4 class="text-center display-4 text-black ">
			    Email:
			</h4>
		</div>
		<div class="container-body  text-center">
			<div class="container bg-light mt-3 mb-3 p-4 aboutHolder" style="display: block;">
			    <h4 class="text-dark text-center mb-0">
			    	<a href="mailto: comunica@ebrasil.art.br" target="_blank">CasaDoConhecimentoSCS@gmail.com</a>
			    	
			    </h4>
			</div>
		</div>
			    <hr>



		
	</div>
</article>

<?php  require_once('includes/footer.php');?>