<?php 

  ini_set('display_errors', 1);
  /*
  // Public API.
  //
  // Possible statuses:
  // 0 -> Pass.
  // 1 -> General error/invalid request.
  // 2 -> Not allowed.
  // 3 -> Empety.
  // 4 -> Already exist.
  // * -> Custom status(es).
 
  */
  require_once('config.php');

  define('PASS', 0);
  define('ERROR', -1);
  define('NOT_ALLOWED', -2);
  define('NOT_AN_IMAGE', -3);
  define('EMPETY', 3);
  define('ALREADY_REGISTERED',4);
#CONSULTAS

	/**
	 * 
	 * @param $tabla refers to a table in the remote database.
	 * @param $columnas refers to a column in the specified table.
	 * @param $valores refers to the target values to be inserted.
	 * @return int
	 */
  	function INSERT($tabla,$columnas,$valores){
		$sql = "INSERT INTO `".$tabla."`(".$columnas.") VALUES (".$valores.")";
		//print $sql;
	    $lookup = $GLOBALS['server']->query($sql);
	    return $lookup->rowCount();
  	}

  	function UPDATE ($tabla,$valores,$donde){
		$sql = "UPDATE `".$tabla."` SET ".$valores." WHERE ".$donde;
	    $lookup = $GLOBALS['server']->query($sql);
	    return $lookup->rowCount();
  	}

  	function DELETE ($tabla,$donde){
	    $sql = "DELETE FROM ".$tabla." WHERE ".$donde;
	    $lookup = $GLOBALS['server']->query($sql);
	    return $lookup->rowCount();
  	}
  	function Consulta($sql){
	    if($lookup = $GLOBALS['server']->query($sql)){
	        return json_encode($lookup->fetchAll());
	    }else{return ERROR;}
  	}
#VERIFICAR
  	function Existe($sql){
	    $valor = false;
	    if ($lookup = $GLOBALS['server']->query($sql)) {
	      $valor =  $lookup->rowCount()==1;
	    }
	    return $valor;
  	}
  	function Admin(){
	    $valor = false;
	    if (isset($_SESSION["dni"])) {
	      $sql = 'SELECT DNI FROM '.$GLOBALS['tables']['admin'].' WHERE DNI = '.$_SESSION['dni'];
	      $valor = Existe($sql);
	    }
	    return $valor;
  	}
#FUNCIONES
  	function Month($fecha){
	    $meses = array('January' => 1,
	                'February' => 2,
	                'March' => 3,
	                'April' => 4,
	                'May' => 5,
	                'June' => 6,
	                'July' => 7,
	                'August' => 8,
	                'September' => 9,
	                'October' => 10, 
	                'November'  => 11,
	                'December' => 12);
	    $aux="'".$fecha."'";
	    if (count(explode(",",$fecha))==2) {
	      $aux = explode(",",$fecha)[0].explode(",",$fecha)[1];
	      $aux = explode(" ",$aux);
	      $aux="'".$aux[2]."-".$meses[$aux[1]]."-".$aux[0]."'";
	    }
	    return $aux;
  	}

  	function dia($dia){
	    switch ($dia) {
	      case 'monday':
	        $dia = "Lunes";
	      break;
	      case 'tuesday':
	        $dia = "Martes";
	      break;
	      case 'wednesday':
	        $dia = "Miercoles";
	      break;
	      case 'thursday':
	        $dia = "Jueves";
	      break;
	      case 'friday':
	        $dia = "Viernes";
	      break;
	      case 'saturday':
	        $dia = "Sabado";
	      break;
	      case 'sunday':
	        $dia = "Domingo";
	      break;
	      return $dia;
	    }
  	}
#CODIGO
  if (isset($_POST['request']))
  {
	session_start();

    if (isset($_POST['content']) && is_array($_POST['content']))
    {
      	switch ($_POST['request']) {
      		case 'getonelinheaactividades':
      			if (count($_POST['content']==1)) {
      				$id = $_POST['content'][0];
      				$sql = 'SELECT 	l.ID_Linhea as "IDLinhea",
      								l.Linhea_Name as "Linhea",
      								l.Name as "Name",
      								COUNT(a.*) as "Actividades"
      						FROM '.$GLOBALS["tables"]["linheas"].' l
      						LEFT JOIN '.$GLOBALS["tables"]["acti"].' a 
      						ON a.ID_Linhea = l.ID_Linhea
      						WHERE l.ID_Linhea = '.$id;
      				if ($lookup=$GLOBALS["server"]->query($sql)) {
      					$linhea = $lookup->fetchAll();
      					$sql = 'SELECT 	ID_Actividad as "ID",
      									Nombre as "Name",
      									Duracion as "Duracion",
      									Color as "Color",a
      									Beneficios as "Beneficios",
      									Dinamica as "Dinamica"
      							FROM '.$GLOBALS["tables"]["acti"].'
			      						WHERE ID_Linhea = '.$id;
			      		if ($lookup = $GLOBALS["server"]->query($sql)) {
			      			$actividades = $lookup->fetchall();

			      		}else{print ERROR;}
      				}else{print ERROR;}
      			}else{print NOT_ALLOWED;}
      		break;
			case 'addPost':
				$title 	 = $_POST['content'][0];
				$author  = $_POST['content'][1];
				$content = $_POST['content'][2];

				if (isset($_FILES['thumbnail'])) {
					if (!file_exists(TARGET_THUMBNAIL_PATH)) {
																// M O  G  E
						mkdir(TARGET_THUMBNAIL_PATH, 0660, true); // --rw-rw----
					}

					$isImage = (getimagesize($_FILES['thumbnail']['tmp_name']) !== false ? true : false);

					if ($isImage) {
						$thumb = mt_rand($system['rng_min'], $system['rng_max']) . // We need to build the extension part again.
												'.' . pathinfo($_FILES['thumbnail']['name'], PATHINFO_EXTENSION);

						if (file_exists(TARGET_THUMBNAIL_PATH . $thumb)) {
							while (file_exists(TARGET_THUMBNAIL_PATH . $thumb)) {
								$thumb = mt_rand($system['rng_min'], $system['rng_max']) . // We need to build the extension part again.
												'.' . pathinfo($_FILES['thumbnail']['name'], PATHINFO_EXTENSION);
							}
						}

						move_uploaded_file($_FILES['thumbnail']['tmp_name'], TARGET_THUMBNAIL_PATH . $thumb);
					} else {
						print NOT_AN_IMAGE;

						break;
					}
				}

				$result = 
				INSERT (
					$GLOBALS['tables']['posts'], '
					
					post_title,
					post_thumbnail, 
					post_content, 
					post_author', 
					
(empty($title)	 ?	'NULL'								: $GLOBALS['server']->quote($title))	. ', ' .
(isset($thumb)	 ? 	$GLOBALS['server']->quote($thumb) 	: 'NULL') 								. ', ' .
(empty($content) ?	'NULL' 								: $GLOBALS['server']->quote($content))	. ', ' .
					$GLOBALS['server']->quote($_POST['content'][1])
				);

				if ($result > 0 && $result < 2)
				{
					print PASS;
				} else {
					print ERROR;
				}

			break;
			case 'modPost':
				define('NO_CHANGES', 4);

				$id		 = $_POST['content'][0];
				$title 	 = $_POST['content'][1];
				$content = $_POST['content'][2];
				$target  = $_POST['content'][3];

				$query = '
				post_title = ' .
(empty($title)	 ?	'NULL'								: $GLOBALS['server']->quote($title))	. ', 
				post_target = ' .
(empty($target)	 ?	'NULL'								: $GLOBALS['server']->quote($target))	. ', 
				post_content = ' .
(empty($content) ?	'NULL' 								: $GLOBALS['server']->quote($content));

				if (isset($_FILES['thumbnail'])) {
					if (!file_exists(TARGET_THUMBNAIL_PATH)) {
																  // M O  G  E
						mkdir(TARGET_THUMBNAIL_PATH, 0660, true); // --rw-rw----
					}

					$isImage = (getimagesize($_FILES['thumbnail']['tmp_name']) !== false ? true : false);

					if ($isImage) {
						$thumb = mt_rand($system['rng_min'], $system['rng_max']) . // We need to build the extension part again.
												'.' . pathinfo($_FILES['thumbnail']['name'], PATHINFO_EXTENSION);

						if (file_exists(TARGET_THUMBNAIL_PATH . $thumb)) {
							while (file_exists(TARGET_THUMBNAIL_PATH . $thumb)) {
								$thumb = mt_rand($system['rng_min'], $system['rng_max']) . // We need to build the extension part again.
												'.' . pathinfo($_FILES['thumbnail']['name'], PATHINFO_EXTENSION);
							}
						}

						move_uploaded_file($_FILES['thumbnail']['tmp_name'], TARGET_THUMBNAIL_PATH . $thumb);

						$query .= ',
					post_thumbnail = ' .
(isset($thumb)	 ? 	$GLOBALS['server']->quote($thumb) 	: 'NULL');
					} else {
						print NOT_AN_IMAGE;

						break;
					}
				}

				$result = 
				UPDATE ($GLOBALS['tables']['posts'], $query, 'post_id = ' . $GLOBALS['server']->quote($id));

				if ($result == 0)
				{
					print NO_CHANGES;
				} elseif ($result > 0 & $result < 2) {
					print PASS;
				} else {
					print ERROR;
				}

				break;
			case 'delPost':
				if (DELETE(
					$GLOBALS['tables']['posts'],
					
					'post_id = ' . $GLOBALS['server']->quote($_POST['content'][0])
				) > 0)
				{
					print PASS;
				} else {
					print ERROR;
				}

				break;
			case 'getPostFromId':
				$result = $GLOBALS['server']->query('SELECT *
														FROM ' . $GLOBALS['tables']['posts'] . '
														WHERE
															post_id = ' . $server->quote($_POST['content'][0]));

				if ($result) {
					print json_encode($result->fetch());
				} else {
					print ERROR;
				}

				break;
			case 'getPinFromId':
				$result = $GLOBALS['server']->query('SELECT *
														FROM ' . $GLOBALS['tables']['pins'] . '
														WHERE
															pin_id = ' . $server->quote($_POST['content'][0]));

				if ($result) {
					print json_encode($result->fetch());
				} else {
					print ERROR;
				}

				break;
			case 'addPin':
				$title 	 = $_POST['content'][0];
				$target	 = $_POST['content'][1];
				$summary = $_POST['content'][2];

				if (isset($_FILES['thumbnail'])) {
					if (!file_exists(TARGET_THUMBNAIL_PATH)) {
																  // M O  G  E
						mkdir(TARGET_THUMBNAIL_PATH, 0660, true); // --rw-rw----
					}

					$isImage = (getimagesize($_FILES['thumbnail']['tmp_name']) !== false ? true : false);

					if ($isImage) {
						$thumb = mt_rand($system['rng_min'], $system['rng_max']) . // We need to build the extension part again.
												'.' . pathinfo($_FILES['thumbnail']['name'], PATHINFO_EXTENSION);

						if (file_exists(TARGET_THUMBNAIL_PATH . $thumb)) {
							while (file_exists(TARGET_THUMBNAIL_PATH . $thumb)) {
								$thumb = mt_rand($system['rng_min'], $system['rng_max']) . // We need to build the extension part again.
												'.' . pathinfo($_FILES['thumbnail']['name'], PATHINFO_EXTENSION);
							}
						}

						move_uploaded_file($_FILES['thumbnail']['tmp_name'], TARGET_THUMBNAIL_PATH . $thumb);
					} else {
						print NOT_AN_IMAGE;

						break;
					}
				}

				$result = 
				INSERT (
					$GLOBALS['tables']['pins'], '
					
					pin_title,
					pin_thumbnail, 
					pin_summary,
					pin_target',
					
					$GLOBALS['server']->quote($title)				. ', ' .
(isset($thumb)	 ? 	$GLOBALS['server']->quote($thumb) 	: 'NULL') 	. ', ' .
					$GLOBALS['server']->quote($summary)				. ', ' .
					$GLOBALS['server']->quote($target)
				);

				if ($result > 0 && $result < 2)
				{
					print PASS;
				} else {
					print ERROR;
				}

				break;
			case 'modPin':
				define('NO_CHANGES', 4);

				$id		 = $_POST['content'][0];
				$title 	 = $_POST['content'][1];
				$summary = $_POST['content'][2];
				$target  = $_POST['content'][3];

				$query = '
					pin_title = ' .
(empty($title)	 ?	'NULL'								: $GLOBALS['server']->quote($title))	. ', 
	pin_summary = ' .
(empty($target)  ?	'NULL' 								: $GLOBALS['server']->quote($summary))	. ', 
	pin_target = ' .
(empty($target)  ?	'NULL' 								: $GLOBALS['server']->quote($target));

				if (isset($_FILES['thumbnail'])) {
					if (!file_exists(TARGET_THUMBNAIL_PATH)) {
																  // M O  G  E
						mkdir(TARGET_THUMBNAIL_PATH, 0660, true); // --rw-rw----
					}

					$isImage = (getimagesize($_FILES['thumbnail']['tmp_name']) !== false ? true : false);

					if ($isImage) {
						$thumb = mt_rand($system['rng_min'], $system['rng_max']) . // We need to build the extension part again.
												'.' . pathinfo($_FILES['thumbnail']['name'], PATHINFO_EXTENSION);

						if (file_exists(TARGET_THUMBNAIL_PATH . $thumb)) {
							while (file_exists(TARGET_THUMBNAIL_PATH . $thumb)) {
								$thumb = mt_rand($system['rng_min'], $system['rng_max']) . // We need to build the extension part again.
												'.' . pathinfo($_FILES['thumbnail']['name'], PATHINFO_EXTENSION);
							}
						}

						move_uploaded_file($_FILES['thumbnail']['tmp_name'], TARGET_THUMBNAIL_PATH . $thumb);
						
						$query .= ',
					pin_thumbnail = ' .
(isset($thumb)	 ? 	$GLOBALS['server']->quote($thumb) 	: 'NULL');

					} else {
						print NOT_AN_IMAGE;

						break;
					}
				}

				$result = UPDATE($GLOBALS['tables']['pins'], $query, 'pin_id = ' . $GLOBALS['server']->quote($id));

				if ($result == 0)
				{
					print NO_CHANGES;
				} elseif ($result > 0 & $result < 2) {
					print PASS;
				} else {
					print ERROR;
				}

				break;
			case 'delPin':
				if (DELETE(
					$GLOBALS['tables']['pins'],
					
					'pin_id = ' . $GLOBALS['server']->quote($_POST['content'][0])
				) > 0)
				{
					print PASS;
				} else {
					print ERROR;
				}

				break;
			case 'addArticle':
				$title 	  = $_POST['content'][0];
				$author   = $_POST['content'][1];
				$content  = $_POST['content'][2];
				$keywords = $_POST['content'][3];

				if (isset($_FILES['thumbnail'])) {
					if (!file_exists(TARGET_THUMBNAIL_PATH)) {
																  // M O  G  E
						mkdir(TARGET_THUMBNAIL_PATH, 0660, true); // --rw-rw----
					}

					$isImage = (getimagesize($_FILES['thumbnail']['tmp_name']) !== false ? true : false);

					if ($isImage) {
						$thumb = mt_rand($system['rng_min'], $system['rng_max']) . // We need to build the extension part again.
												'.' . pathinfo($_FILES['thumbnail']['name'], PATHINFO_EXTENSION);

						if (file_exists(TARGET_THUMBNAIL_PATH . $thumb)) {
							while (file_exists(TARGET_THUMBNAIL_PATH . $thumb)) {
								$thumb = mt_rand($system['rng_min'], $system['rng_max']) . // We need to build the extension part again.
												'.' . pathinfo($_FILES['thumbnail']['name'], PATHINFO_EXTENSION);
							}
						}

						move_uploaded_file($_FILES['thumbnail']['tmp_name'], TARGET_THUMBNAIL_PATH . $thumb);
					} else {
						print NOT_AN_IMAGE;

						break;
					}
				}

				$result = 
				INSERT (
					$GLOBALS['tables']['articles'], '
					
					article_title,
					article_thumbnail, 
					article_content, 
					article_author,
					article_keywords', 
					
(empty($title)	 ?	'NULL'								: $GLOBALS['server']->quote($title))	. ', ' .
(isset($thumb)	 ? 	$GLOBALS['server']->quote($thumb) 	: 'NULL') 								. ', ' .
(empty($content) ?	'NULL' 								: $GLOBALS['server']->quote($content))	. ', ' .
					$GLOBALS['server']->quote($author)											. ', ' .
					$GLOBALS['server']->quote($keywords)
				);

				if ($result > 0 && $result < 2)
				{
					print PASS;
				} else {
					print ERROR;
				}

			break;
			case 'modArticle':
				define('NO_CHANGES', 4);

				$id		  = $_POST['content'][0];
				$title 	  = $_POST['content'][1];
				$content  = $_POST['content'][2];
				$keywords = $_POST['content'][3];

				$query = '
				article_title = ' .
(empty($title)	  ?	'NULL'								: $GLOBALS['server']->quote($title))	. ', 
article_content = ' .
(empty($content)  ?	'NULL' 								: $GLOBALS['server']->quote($content)) 	. ', 
article_keywords = ' .					
(empty($keywords) ?	'NULL' 								: $GLOBALS['server']->quote($keywords));

				if (isset($_FILES['thumbnail'])) {
					if (!file_exists(TARGET_THUMBNAIL_PATH)) {
																	// M O  G  E
						mkdir(TARGET_THUMBNAIL_PATH, 0660, true); // --rw-rw----
					}

					$isImage = (getimagesize($_FILES['thumbnail']['tmp_name']) !== false ? true : false);

					if ($isImage) {
						$thumb = mt_rand($system['rng_min'], $system['rng_max']) . // We need to build the extension part again.
												'.' . pathinfo($_FILES['thumbnail']['name'], PATHINFO_EXTENSION);

						if (file_exists(TARGET_THUMBNAIL_PATH . $thumb)) {
							while (file_exists(TARGET_THUMBNAIL_PATH . $thumb)) {
								$thumb = mt_rand($system['rng_min'], $system['rng_max']) . // We need to build the extension part again.
												'.' . pathinfo($_FILES['thumbnail']['name'], PATHINFO_EXTENSION);
							}
						}

						move_uploaded_file($_FILES['thumbnail']['tmp_name'], TARGET_THUMBNAIL_PATH . $thumb);

						$query .= ',
				article_thumbnail = ' .
(isset($thumb)	  ?	$GLOBALS['server']->quote($thumb) 	: 'NULL');
					} else {
						print NOT_AN_IMAGE;

						break;
					}
				}

				$result = UPDATE($GLOBALS['tables']['articles'], $query, 'article_id = ' . $GLOBALS['server']->quote($id));

				if ($result == 0)
				{
					print NO_CHANGES;
				} elseif ($result > 0 & $result < 2) {
					print PASS;
				} else {
					print ERROR;
				}

				break;
			case 'delArticle':
				if (DELETE(
					$GLOBALS['tables']['articles'],
					
					'article_id = ' . $GLOBALS['server']->quote($_POST['content'][0])
				) > 0)
				{
					print PASS;
				} else {
					print ERROR;
				}

				break;
			case 'getArticleFromId':
				$result = $GLOBALS['server']->query('SELECT *
														FROM ' . $GLOBALS['tables']['articles'] . '
														WHERE
															article_id = ' . $server->quote($_POST['content'][0]));

				if ($result) {
					print json_encode($result->fetch());
				} else {
					print ERROR;
				}

				break;
#DESCRIPCION
          //ACTUALIZO LA DESCRIPCION
      		case 'setDescripcion':
            if (count($_POST['content'])==2) {
              $txt = "txt/".$_POST['content'][1].".txt";
              if (!file_exists($txt)) {
                $txt = "../txt/".$_POST['content'][1].".txt";
              }
              if (file_exists($txt)) {
                //GUARDO LA NUEVA DESCRIPCION
                $nuevaDescripcion = $_POST['content'][0];
                $archivo = fopen($txt,"w");
                //ESCRIBO EN EL ARCHIVO
                fwrite($archivo, $nuevaDescripcion);
                fclose($archivo);
                print PASS;
              }else{print ERROR;}//ERROR SI EL ARCHIVO NO EXISTE
            }else{print NOT_ALLOWED;}//NO PERMITIDO SI NO LLEGARON LOS DATOS CORRECTOS
      		break;
          //OBTENGO UNA DESCRIPCION
          case 'getDescripcion':
            if (count($_POST['content'])==1) {
              $txt = "txt/".$_POST['content'][0].".txt";
              if (!file_exists($txt)) {
                $txt = "../txt/".$_POST['content'][0].".txt";
              }
              if (file_exists($txt)) {
                $archivo = fopen($txt,"r");
                $txt = '';
                //RECORRO EL TXT Y GUARDO LOS DATOS
                while (!feof($archivo)) {
                  $txt .= fgets($archivo,140);
                }
                fclose($archivo);
                //DEVUELVO EL TXT
                print $txt;
              }else{print ERROR;}//ERROR SI EL ARCHIVO NO EXISTE
            }else{print NOT_ALLOWED;}//NO PERMITIDO SI NO LLEGARON LOS DATOS CORRECTOS
          break;
         
#DEFAULT
        	default:
        		print ERROR;
      }
    } elseif (isset($_POST['request'])) {
      	switch ($_POST['request']) {
			case 'getPostsForManager':
				$sql = 'SELECT
														post_id,
														post_title, 
														post_datetime, 
														post_author,
														post_target
													 FROM ' . $GLOBALS['tables']['posts'];
				print(Consulta($sql));
			break;
			case 'getArticlesForManager':
				$sql = 'SELECT
														article_id,
														article_title, 
														article_datetime, 
														article_author
													 FROM ' . $GLOBALS['tables']['articles'];
				print(Consulta($sql));
			break;
			case 'getPinsForManager':
				$sql='SELECT
														pin_id,
														pin_title, 
														pin_summary, 
														pin_target
														FROM ' . $GLOBALS['tables']['pins'];
				print(Consulta($sql));
			break;
			case 'getPinsForHome':
				$sql='SELECT
														pin_title, 
														pin_thumbnail, 
														pin_summary, 
														pin_target
													 FROM ' . $GLOBALS['tables']['pins'];
				print(Consulta($sql));
			break;
			case 'getPostsForHome':
				$sql='SELECT
														post_id,
														post_title, 
														post_thumbnail, 
														post_content, 
														post_datetime,
														post_author,
														post_target
														FROM ' . $GLOBALS['tables']['posts'];
				if($lookup = $GLOBALS['server']->query($sql)){
					$datos = $lookup->fetchAll();
					for ($i=0; $i < count($datos); $i++) { 
						if (isset($datos[$i]['post_id'])) {
							$_SESSION['"'.$datos[$i]['post_id'].'"']=$datos[$i]['post_thumbnail'];
						}
					}
					print(Consulta($sql));
			    }else{return ERROR;}
			break;
			case 'getArticlesForHome':
				$sql='SELECT
														article_id,
														article_title, 
														article_thumbnail, 
														article_content,
														article_author,
														article_keywords
														FROM ' . $GLOBALS['tables']['articles'];
				print(Consulta($sql));
			break;
        	default:
          		print ERROR;
      }
    }
  } else {
    $title = 'API'; $render = true;
    require_once('includes/header.php');
    print '
    <div id="vcentered_message" class="row valign-wrapper">
      <div class="col s12 center-align black-text">
        <h5>
          <i class="large material-icons">code</i> <br>
            ¡Hola! Esta es nuestra API.
          </h5>
      </div>
    </div>';
    require_once('includes/footer.php');
  }
  
  $title = 'API'; $no_menu = true;
?>