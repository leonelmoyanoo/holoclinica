<?php $render = true; $scripts = [ 'admin/common' ];

require_once('includes/admin/enumerator.php'); require_once('includes/header.php');

function getDashboardMenuItem($internal, $title, $icon) {
    // TODO: For some reason, the first item has an extra space at the beginning.
    //       Review this as soon as possible.
    
    print '
    <li class="nav-item border-bottom">
        <a class="nav-link active" href="?menu=' . $internal . '">
          <span><i class="fa fa-' . $icon . ' pr-2"></i></span>' . $title . '
        </a>
    </li>';
    // TODO: The following snippet should be added back to
    //       support highlighting the current item.
    // 
    //       <span class="sr-only">(current)</span>
}

$options = array (
    array (
      'internal' => MENU_ITEM_HOME,
      'title'    => 'Inicio',
      'icon'     => 'home'
    ),
    array (
      'internal' => MENU_ITEM_IMPORTANT,
      'title'    => 'Curaduria de conteudo',
      'icon'     => 'bookmark'
    ),
    array (
      'internal' => MENU_ITEM_POSTS,
      'title'    => 'Nossas atividades',
      'icon'     => 'file-text'
    ),
    array (
      'internal' => MENU_ITEM_ARTICLES,
      'title'    => 'Artigos/blog',
      'icon'     => 'newspaper-o'
    ),
    array (
      'internal' => MENU_ITEM_USERS,
      'title'    => 'Usuários',
      'icon'     => 'user'
    ),
    array (
      'internal' => MENU_ITEM_QUESTIONS,
      'title'    => 'Preguntas',
      'icon'     => 'phone-square'
    ),
    array (
      'internal' => MENU_ITEM_HELP,
      'title'    => 'Ajuda',
      'icon'     => 'question-circle'
    )
);

?>

<div class="container-fluid">
  <div class="row bg-light py-3 rounded shadow-sm">
    <nav class="col-md-3 d-none d-md-block bg-light sidebar">
      <div class="sidebar-sticky">
        <ul class="nav flex-column">
            <!-- Menu items are generated on runtime here. -->
            <?php
                foreach ($options as $option) {
                    print getDashboardMenuItem($option['internal'], $option['title'], $option['icon']);
                }
            ?>  
        </ul>
      </div>
    </nav>

    <div role="main" class="col-md-9 ml-sm-auto col-lg-9 px-4">
      <?php
        $menu = (!isset($_GET['menu']) || empty($_GET['menu']) ? MENU_ITEM_HOME : $_GET['menu']);

        switch ($menu) {
          case MENU_ITEM_HOME: // MENU_ITEM_HOME, or empty, or null.
            $scripts[] = 'admin/home';

            print '
            <canvas class="my-4 w-100" id="myChart" width="900" height="380"></canvas>

            <h2>Section title</h2>
            <div class="table-responsive">
              <table class="table table-striped table-sm">
                <thead>
                  <tr>
                    <th>#</th>
                    <th>Header</th>
                    <th>Header</th>
                    <th>Header</th>
                    <th>Header</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td>1,001</td>
                    <td>Lorem</td>
                    <td>ipsum</td>
                    <td>dolor</td>
                    <td>sit</td>
                  </tr>
                  <tr>
                    <td>1,002</td>
                    <td>amet</td>
                    <td>consectetur</td>
                    <td>adipiscing</td>
                    <td>elit</td>
                  </tr>
                  <tr>
                    <td>1,003</td>
                    <td>Integer</td>
                    <td>nec</td>
                    <td>odio</td>
                    <td>Praesent</td>
                  </tr>
                  <tr>
                    <td>1,003</td>
                    <td>libero</td>
                    <td>Sed</td>
                    <td>cursus</td>
                    <td>ante</td>
                  </tr>
                  <tr>
                    <td>1,004</td>
                    <td>dapibus</td>
                    <td>diam</td>
                    <td>Sed</td>
                    <td>nisi</td>
                  </tr>
                  <tr>
                    <td>1,005</td>
                    <td>Nulla</td>
                    <td>quis</td>
                    <td>sem</td>
                    <td>at</td>
                  </tr>
                  <tr>
                    <td>1,006</td>
                    <td>nibh</td>
                    <td>elementum</td>
                    <td>imperdiet</td>
                    <td>Duis</td>
                  </tr>
                  <tr>
                    <td>1,007</td>
                    <td>sagittis</td>
                    <td>ipsum</td>
                    <td>Praesent</td>
                    <td>mauris</td>
                  </tr>
                  <tr>
                    <td>1,008</td>
                    <td>Fusce</td>
                    <td>nec</td>
                    <td>tellus</td>
                    <td>sed</td>
                  </tr>
                  <tr>
                    <td>1,009</td>
                    <td>augue</td>
                    <td>semper</td>
                    <td>porta</td>
                    <td>Mauris</td>
                  </tr>
                  <tr>
                    <td>1,010</td>
                    <td>massa</td>
                    <td>Vestibulum</td>
                    <td>lacinia</td>
                    <td>arcu</td>
                  </tr>
                  <tr>
                    <td>1,011</td>
                    <td>eget</td>
                    <td>nulla</td>
                    <td>Class</td>
                    <td>aptent</td>
                  </tr>
                  <tr>
                    <td>1,012</td>
                    <td>taciti</td>
                    <td>sociosqu</td>
                    <td>ad</td>
                    <td>litora</td>
                  </tr>
                  <tr>
                    <td>1,013</td>
                    <td>torquent</td>
                    <td>per</td>
                    <td>conubia</td>
                    <td>nostra</td>
                  </tr>
                  <tr>
                    <td>1,014</td>
                    <td>per</td>
                    <td>inceptos</td>
                    <td>himenaeos</td>
                    <td>Curabitur</td>
                  </tr>
                  <tr>
                    <td>1,015</td>
                    <td>sodales</td>
                    <td>ligula</td>
                    <td>in</td>
                    <td>libero</td>
                  </tr>
                </tbody>
              </table>
            </div>';

            break;
          case MENU_ITEM_IMPORTANT:
            $scripts[] = 'admin/important';

            print '                        
            <div class="alert alert-info" role="alert">
            Crie até seis itens que serão exibidos na página principal.
            </div>

            <div id="pinsLoader" class="d-flex justify-content-center">
              <div class="spinner-border text-warning mx-auto" role="status">
                <span class="sr-only">Carregando ...</span>
              </div>
            </div>
            <button id="addPin" class="btn btn-primary" onclick="addPin()" style="display: none">Adicionar marca</button>
            
            <!-- Modal: pin creation -->
            <div class="modal fade" id="pinCreationModal" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="pinCreationModalLabel" aria-hidden="true">
              <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <h5 class="modal-title" id="pinCreationModalLabel">Adicionar marca</h5>
                    <button type="button" class="close closePinCreationModal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                  </div>
                  <div class="modal-body">
                    <form>
                      <div class="form-group">
                        <label for="pinTitle">Título</label>
                        <input type="text" class="form-control" id="pinTitle" placeholder="Uma nova linha é criada." required>
                      </div>
                      <div class="form-group">
                        <label for="fileBox">Vista prévia</label>
                        <div id="fileBox" class="custom-file">
                          <input type="file" class="custom-file-input image-input" accept="image/*" id="pinThumbnail">
                          <label class="custom-file-label" for="pinThumbnail">Selecionar arquivo</label>
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="pinTarget">Destino</label>
                        <input type="text" class="form-control" id="pinTarget" placeholder="Ler mais ..." required>
                      </div>
                      <div class="form-group pt-2">
                        <label for="pinSummary">Sumário</label>
                        <textarea class="form-control" id="pinSummary" rows="3"></textarea>
                      </div>
                    </form>
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-secondary closePinCreationModal">Fechar</button>
                    <button id="savePin" type="button" class="btn btn-primary save-button">Guardar mudanças</button>
                  </div>
                </div>
              </div>
            </div>

            <!-- Modal: pin modification -->
            <div class="modal fade" id="pinModificationModal" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="pinCreationModalLabel" aria-hidden="true">
              <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <h5 class="modal-title" id="pinCreationModalLabel">Modificar marca</h5>
                    <button type="button" class="close closePinCreationModal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                  </div>
                  <div class="modal-body">
                    <form>
                      <div class="form-group">
                        <label for="pinModTitle">Título</label>
                        <input type="text" class="form-control" id="pinModTitle" placeholder="Uma nova linha é criada." required>
                      </div>
                      <div class="form-group">
                        <label for="fileBox">Vista prévia</label>
                        <div id="fileBox" class="custom-file">
                          <input type="file" class="custom-file-input image-input" accept="image/*" id="pinModThumbnail">
                          <label class="custom-file-label" for="pinModThumbnail">Selecionar arquivo</label>
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="pinModTarget">Destino</label>
                        <input type="text" class="form-control" id="pinModTarget" placeholder="Ler mais ..." required>
                      </div>
                      <div class="form-group pt-2">
                        <label for="pinModSummary">Sumário</label>
                        <textarea class="form-control" id="pinModSummary" rows="3"></textarea>
                      </div>
                    </form>
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-secondary closePinCreationModal">Fechar</button>
                    <button id="saveModPin" type="button" class="btn btn-primary save-button">Guardar mudanças</button>
                  </div>
                </div>
              </div>

              <input type="hidden" id="pinModId" name="pinModId" value="">
            </div>
            
            <!-- Modal: pin deletion confirmation. -->
            <div class="modal fade" id="deleteConfirmationModal" tabindex="-1" role="dialog" aria-labelledby="staticBackdropLabel" aria-hidden="true">
              <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <h5 class="modal-title" id="staticBackdropLabel">Tem certeza de que deseja excluir esta postagem?</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                  </div>
                  <div class="modal-body">
                    A publicação não estará disponível e não será possível recuperá-la.
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Não</button>
                    <button id="acceptPinDeletion" type="button" class="btn btn-primary">Sim</button>
                  </div>
                </div>
              </div>
            
              <input type="hidden" name="toDeleteId" id="toDeleteId" value="">
            </div>';

            break;
          case MENU_ITEM_ARTICLES:
            $scripts[] = 'admin/articles';

            print '
            <div id="articlesLoader" class="d-flex justify-content-center">
              <div class="spinner-border text-warning mx-auto" role="status">
                <span class="sr-only">Carregando ...</span>
              </div>
            </div>
            <button id="addArticle" class="btn btn-primary" onclick="addArticle()" style="display: none">Adicionar artigo</button>

            <!-- Modal: article creation -->
            <div class="modal fade" id="articleCreationModal" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="articleCreationModalLabel" aria-hidden="true">
              <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <h5 class="modal-title" id="articleCreationModalLabel">Adicionar artigo</h5>
                    <button type="button" class="close closePostCreationModal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                  </div>
                  <div class="modal-body">
                    <form>
                      <div class="form-group">
                        <label for="articleTitle">Título</label>
                        <input type="text" class="form-control" id="articleTitle" placeholder="Uma nova linha é criada." required>
                      </div>
                      <div class="form-group">
                        <label for="articleAuthor">Autor</label>
                        <select class="form-control" id="articleAuthor">
                          <option>user0 (você)</option>
                          <option>user1</option>
                          <option>user2</option>
                          <option>user3</option>
                          <option>user4</option>
                        </select>
                      </div>
                      <div class="form-group">
                        <label for="articleKeywords">Palavras chave</label>
                        <input type="text" class="form-control" id="articleKeywords" placeholder="Separe com espaços" required>
                      </div>
                      <div class="form-group">
                        <label for="fileBox">Vista prévia</label>
                        <div id="fileBox" class="custom-file">
                          <input type="file" class="custom-file-input image-input" accept="image/*" id="articleThumbnail">
                          <label class="custom-file-label" for="articleThumbnail">Selecionar arquivo</label>
                        </div>
                      </div>
                      <div class="form-group pt-2">
                        <label for="articleContent">Conteúdo</label>
                        <textarea class="form-control" id="articleContent" rows="3"></textarea>
                      </div>
                    </form>
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-secondary closeArticleCreationModal">Fechar</button>
                    <button id="saveArticle" type="button" class="btn btn-primary save-button">Guardar mudanças</button>
                  </div>
                </div>
              </div>
            </div>

            <!-- Modal: post modification  -->
            <div class="modal fade" id="articleModificationModal" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="articleCreationModalLabel" aria-hidden="true">
              <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <h5 class="modal-title" id="articleCreationModalLabel">Modificar postagem</h5>
                    <button type="button" class="close closeArticleCreationModal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                  </div>
                  <div class="modal-body">
                    <form>
                      <div class="form-group">
                        <label for="articleModTitle">Título</label>
                        <input type="text" class="form-control" id="articleModTitle" placeholder="Uma nova linha é criada." required>
                      </div>
                      <div class="form-group">
                        <label for="articleModAuthor">Autor</label>
                        <input type="text" class="form-control" id="articleModAuthor">
                      </div>
                      <div class="form-group">
                        <label for="articleModKeywords">Palavras chave</label>
                        <input type="text" class="form-control" id="articleModKeywords" placeholder="Separe com espaços" required>
                      </div>
                      <div class="form-group">
                        <label for="fileBox">Vista prévia</label>
                        <div id="fileBox" class="custom-file">
                          <input type="file" class="custom-file-input image-input" accept="image/*" id="articleModThumbnail">
                          <label class="custom-file-label" for="articleModThumbnail">Selecionar arquivo</label>
                        </div>
                      </div>
                      <div class="form-group pt-2">
                        <label for="articleModContent">Conteúdo</label>
                        <textarea class="form-control" id="articleModContent" rows="3"></textarea>
                      </div>
                    </form>
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-secondary closeArticleCreationModal">Fechar</button>
                    <button id="saveModArticle" type="button" class="btn btn-primary save-button">Guardar mudanças</button>
                  </div>
                </div>
              </div>
              <input type="hidden" id="articleModId" name="articleModId" value="">
            </div>

            <!-- Modal: post deletion confirmation. -->
            <div class="modal fade" id="deleteConfirmationModal" tabindex="-1" role="dialog" aria-labelledby="staticBackdropLabel" aria-hidden="true">
              <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <h5 class="modal-title" id="staticBackdropLabel">Tem certeza de que deseja excluir este artigo?</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                  </div>
                  <div class="modal-body">
                    O artigo não estará disponível e não será possível recuperá-la.
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Não</button>
                    <button id="acceptArticleDeletion" type="button" class="btn btn-primary">Sim</button>
                  </div>
                </div>
              </div>
            
              <input type="hidden" name="toDeleteId" id="toDeleteId" value="">
            </div>';

            break;
          case MENU_ITEM_POSTS:
            $scripts[] = 'admin/posts';

            print '
            <div id="postsLoader" class="d-flex justify-content-center">
              <div class="spinner-border text-warning mx-auto" role="status">
                <span class="sr-only">Carregando ...</span>
              </div>
            </div>
            <button id="addPost" class="btn btn-primary" onclick="addPost()" style="display: none">Adicionar postagem</button>


            <!-- Modal: post creation -->
            <div class="modal fade" id="postCreationModal" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="postCreationModalLabel" aria-hidden="true">
              <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <h5 class="modal-title" id="postCreationModalLabel">Adicionar postagem</h5>
                    <button type="button" class="close closePostCreationModal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                  </div>
                  <div class="modal-body">
                    <form>
                      <div class="form-group">
                        <label for="postTitle">Título</label>
                        <input type="text" class="form-control" id="postTitle" placeholder="Uma nova linha é criada." required>
                      </div>
                      <div class="form-group">
                        <label for="postAuthor">Autor</label>
                        <select class="form-control" id="postAuthor">
                          <option>user0 (você)</option>
                          <option>user1</option>
                          <option>user2</option>
                          <option>user3</option>
                          <option>user4</option>
                        </select>
                      </div>
                      <div class="form-group">
                        <label for="fileBox">Vista prévia</label>
                        <div id="fileBox" class="custom-file">
                          <input type="file" class="custom-file-input image-input" accept="image/*" id="postThumbnail">
                          <label class="custom-file-label" for="postThumbnail">Selecionar arquivo</label>
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="postTarget">Destino</label>
                        <input type="text" class="form-control" placeholder="Em branco para obter a postagem padrão." id="postTarget" required>
                      </div>
                      <div class="form-group pt-2">
                        <label for="postContent">Conteúdo</label>
                        <textarea class="form-control" id="postContent" rows="3"></textarea>
                      </div>
                    </form>
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-secondary closePostCreationModal">Fechar</button>
                    <button id="savePost" type="button" class="btn btn-primary save-button">Guardar mudanças</button>
                  </div>
                </div>
              </div>
            </div>

            <!-- Modal: post modification  -->
            <div class="modal fade" id="postModificationModal" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="postCreationModalLabel" aria-hidden="true">
              <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <h5 class="modal-title" id="postCreationModalLabel">Modificar postagem</h5>
                    <button type="button" class="close closePostCreationModal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                  </div>
                  <div class="modal-body">
                    <form>
                      <div class="form-group">
                        <label for="postModTitle">Título</label>
                        <input type="text" class="form-control" id="postModTitle" placeholder="Uma nova linha é criada." required>
                      </div>
                      <div class="form-group">
                        <label for="postModAuthor">Autor</label>
                        <input type="text" class="form-control" id="postModAuthor">
                      </div>
                      <div class="form-group">
                        <label for="fileBox">Vista prévia</label>
                        <div id="fileBox" class="custom-file">
                          <input type="file" class="custom-file-input image-input" accept="image/*" id="postModThumbnail">
                          <label class="custom-file-label" for="postModThumbnail">Selecionar arquivo</label>
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="postModTarget">Destino</label>
                        <input type="text" class="form-control" placeholder="Em branco para obter a postagem padrão." id="postModTarget" required>
                      </div>
                      <div class="form-group pt-2">
                        <label for="postModContent">Conteúdo</label>
                        <textarea class="form-control" id="postModContent" rows="3"></textarea>
                      </div>
                    </form>
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-secondary closePostCreationModal">Fechar</button>
                    <button id="saveModPost" type="button" class="btn btn-primary save-button">Guardar mudanças</button>
                  </div>
                </div>
              </div>
              <input type="hidden" id="postModId" name="postModId" value="">
            </div>

            <!-- Modal: post deletion confirmation. -->
            <div class="modal fade" id="deleteConfirmationModal" tabindex="-1" role="dialog" aria-labelledby="staticBackdropLabel" aria-hidden="true">
              <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <h5 class="modal-title" id="staticBackdropLabel">Tem certeza de que deseja excluir esta postagem?</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                  </div>
                  <div class="modal-body">
                    A publicação não estará disponível e não será possível recuperá-la.
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Não</button>
                    <button id="acceptPostDeletion" type="button" class="btn btn-primary">Sim</button>
                  </div>
                </div>
              </div>
            
              <input type="hidden" name="toDeleteId" id="toDeleteId" value="">
            </div>';

            break;
        }

      ?>
    </div>
  </div>
</div>

<!-- Modal: warn if user will loose data. -->
<div class="modal fade" id="warnLostDataModal" tabindex="-1" role="dialog" aria-labelledby="staticBackdropLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="staticBackdropLabel">Tem certeza de que deseja sair?</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        Se sair, todas as alterações que você fez serão perdidas.
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Não</button>
        <button id="acceptDataLost" type="button" class="btn btn-primary">Sim</button>
      </div>
    </div>
  </div>
</div>

<?php  require_once('includes/footer.php'); ?>