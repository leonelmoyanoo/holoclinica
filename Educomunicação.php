<?php $render = true;$titulo='Educomunicação';$customHeader = $titulo;require_once('includes/header.php'); $scripts = ['Edu','pins' ]; ?>

<?php $bg='#B4B6B6';include('includes/descripcion.php'); ?>
<hr>
	<article>
			<div>
		  		<div class="container-header rounded-sm">
			    	<h1 class="text-center display-4 text-black ">
			    		Educomunicação: o que é e como usar na sua aula
			    	</h1>
			    </div>
			    <div class="container-body  bg-light mt-3 mb-3 p-4  text-center row">
			    	<div class="col-3">
			            <img src="assets/images/edu/edu_01.png"
			              class="img-fluid" alt="">
			        </div>
			    	<div class="container bg-light mt-3 mb-3 p-4 aboutHolder col-8" style="display: block;">
			    		<h4 class="text-dark text-center mb-0">
			    			A Educomunicação propõe uma intervenção a partir de Educação para a mídia, ou seja, o professor e os estudantes desenvolvem em sala de aula conteúdos educativos, fazendo a gestão democrática das mídias práticas de ecossistemas comunicativos abertos e criativos... <a href="https://novaescola.org.br/conteudo/18177/educomunicacao-o-que-e-e-como-usar-na-sua-aula" target="_blank"><br>Leia mais</a>
			    		</h4>
					    	
			    	</div>

			    </div>
			    

			    
			  </div>
		</article>

	<div class="container my-3">
		<!-- Three columns of text below the carousel -->
		<div id="pinsHolder" class="row"  style="display: none">
			
		</div>
	</div>

<?php  require_once('includes/footer.php');?>
