<?php $render = true;$inicio=true; require_once('includes/header.php'); $scripts = [ 'inicio' ]; ?>

	<div id="indexLoader" class="progress">
		<div class="progress-bar progress-bar-striped progress-bar-animated bg-warning" role="progressbar" aria-valuenow="15" aria-valuemin="0" aria-valuemax="100" style="width: 15%"></div>
	</div>
	<div class="container my-3">
		<!-- Three columns of text below the carousel -->
		<div id="pinsHolder" class="row"  style="display: none">
			
		</div>
	</div>

	<div class="container bg-light" id="postsHolder" style="display: none;">
	  <div class="display-3 text-center mb-3">Conheça nossas atividades</div>

      <div class="row">
	  		<!-- Second section -->
	  </div>
	</div>
	
	<div class="container bg-light" id="articlesHolder" style="display: none">
	  <div class="display-3 text-center mb-3 p-3">Blog</div>
	  	<h4>Este espaço será destinado as postagens dos participantes da Casa do Conhecimento</h4>
	  </div>
	</div>

	<!-- Modal -->
	<div class="modal fade" id="postViewer" tabindex="-1" role="dialog" aria-labelledby="postViewer" aria-hidden="true">
		<div class="modal-dialog modal-lg" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="postTitle">Nenhuma postagem selecionada</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body" id="postContent">Nada por aqui.</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-primary" data-dismiss="modal">Sair</button>
				</div>
			</div>
		</div>
	</div>
<?php  require_once('includes/footer.php');?>
