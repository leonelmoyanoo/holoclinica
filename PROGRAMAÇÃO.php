<?php $render = true;$titulo='PROGRAMAÇÃO';$customHeader = $titulo;require_once('includes/header.php'); $scripts = [ 'Programacao' ]; ?>


<table class="table">
    <thead>
      <tr>
      	<th scope="col"></th>
        <th scope="col">Programação diaria</th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <th scope="row">12h</th>
        <td>Almoço</td>
      </tr>
      <tr>
        <th scope="row">13h</th>
        <td>Recepção com Musica e descanso pos almoço</td>
      </tr>
      <tr class="table-warning">
        <th scope="row">13h30</th>
        <td>Practica de Conciencia Corporal e Meditação</td>
      </tr>
      <tr class="table-warning">
        <th scope="row">13h45</th>
        <td>Minha Lição de Casa</td>
      </tr>
      <tr class="table-warning">
        <th scope="row">14</th>
        <td>Fazer as tarefas de casa</td>
      </tr>
      <tr class="table-warning">
        <th scope="row">15h30</th>
        <td>Lanche Break</td>
      </tr>
      <tr class="table-success">
        <th scope="row">16h</th>
        <td>Oficina de Roteirização (escolha da materia do dia)</td>
      </tr>
      <tr class="table-success">
        <th scope="row">17h</th>
        <td>Gravação com camera e tecnica de apresentação</td>
      </tr>
      <tr class="table-success">
        <th scope="row">18h</th>
        <td>Encerramento das Atividades</td>
      </tr>
    </tbody>
  </table>


  <hr>

<table class="table">
    <thead>
      <tr>
      	<th scope="col"></th>
        <th scope="col">Oficinas alternativas simultaneas de 16h a 18h</th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <th scope="row">2a Feira</th>
        <td>Gastronomia</td>
      </tr>
      <tr>
        <th scope="row">3a Feira</th>
        <td>Artes Cenicas e Expressão Vocal</td>
      </tr>
      <tr>
        <th scope="row">4a Feira</th>
        <td>Dança Clipe e Conversação</td>
      </tr>
      <tr>
        <th scope="row">5a Feira</th>
        <td>Desenho e Artes Plasticas</td>
      </tr>
      <tr>
        <th scope="row">6a Feira</th>
        <td>Musica e Pratica</td>
      </tr>
    </tbody>
  </table>
<?php  require_once('includes/footer.php');?>