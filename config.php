<?php
    $system = array(
        'debug'         => true,            // In debug mode, we won't display nor abort execution on database errors.
        'rng_min'       => 0,               // Minimum number in the random pool.
        'rng_max'       => mt_getrandmax()  // Maximum number in the random pool.
    );

    $info = array(
        'area_name'     => 'Base',
        'company_name'  => 'Cheiro di Mato',
        'home'          => 'index.html'
    );
    //array to convert numbers into positions
    $positions = array(
        1 => 'First',
        2 => 'Second',
        3 => 'Third',
        4 => 'Fourth',
        5 => 'Fifth',
        6 => 'Sixth',
        7 => 'Seventh',
        8 => 'Eighth',
        9 => 'Nineth'
    );
    // Database
    $GLOBALS['server']  = array(
        'hostname'      => 'localhost',
        'username'      => 'buffrapp',
        'password'      => 'buffrappar2019rarlt',
        'port'          => 3306,
        'database'      => 'Cheiro-di-Mato'
    );

    $GLOBALS['tables']  = array(
        'users'         => 'Cheiro_di_Mato_users',
        'posts'         => 'Cheiro_di_Mato_posts',
        'pins'          => 'Cheiro_di_Mato_pinned',
        'articles'      => 'Cheiro_di_Mato_articles',
        'acti'          => 'Cheiro_di_Mato_actividades',
        'acti_m'        => 'Cheiro_di_Mato_actividades_materiales',
        'linheas'       => 'Cheiro_di_Mato_linheas',
        'horarios'      => 'Cheiro_di_Mato_shedule',
        'horarios_e'    => 'Cheiro_di_Mato_schedule_empleados',
        'redes_e'       => 'Cheiro_di_Mato_social_empleados',
        'equipo'        => 'Cheiro_di_Mato_Team'
    );

    $puntos = file_exists('vendor/autoload.php')?'':'../';
    //if (file_exists($puntos.'vendor/autoload.php')) {
        //require_once($puntos.'vendor/autoload.php');
        // Open a connection to the database.
        try {
            $GLOBALS['server'] =  new PDO('mysql:host=' . $server['hostname'] . ':' . $server['port'] . ';dbname=' . $server['database'] .';charset=utf8', $server['username'], $server['password']);
        } catch (PDOException $e) {
            if (!$system['debug']) {
              $render = true; $secure = false; $title = 'Error fatal';
              $error = '
                <div class="row">
                  <div class="col-12">
                      <h5 class="grey-text text-center text-white">
                          <!-- <i class="large material-icons">link_off</i> <br> Look for a replacement icon pack. -->
                          Não foi possível conectar ao banco de dados. Deseja <a href="">tentar de novo</a>?
                      </h5>
                  </div>
                </div>';
              require_once($puntos.'includes/header.php'); // The header file can be provided an error which shall mean
                                                  // "abort when you're done" and display the message.
                                                  //
                                                  // TODO: Review this behavior.
              require_once($puntos.'includes/footer.php');
              $server = null;
              error_log($e->getMessage());
            }
        }
   /* } else {
        exit('Failed to load composer, please download all the required modules using the following command and try again.<br>
              <br>
              Não foi possivel carregar composer, por favor faça a descarga dos modulos necesarios usando o siguiente comando e tente novamente.<br>
              <br>
              <pre>composer install</pre>');
    }*/
    
    define('TARGET_THUMBNAIL_PATH', 'private/assets/images/');
    define('ANIMATION_TIME', 400); // ms
?>
