<?php $render = true;$customHeader=true; require_once('includes/header.php'); $scripts = [ 'view' ]; ?>
    <input type="hidden" id="selectedArticle" value="<?= (isset($_GET['articleId']) ? $_GET['articleId'] : 0) ?>">
	<div id="indexLoader" class="progress">
		<div class="progress-bar progress-bar-striped progress-bar-animated bg-warning" role="progressbar" aria-valuenow="15" aria-valuemin="0" aria-valuemax="100" style="width: 15%"></div>
	</div>
	<div class="container-sm bg-light mt-3 mb-3 p-4" id="articleContent">
	</div>
<?php  require_once('includes/footer.php');?>
