//PAGINA ACTUAL
var page = 'general';

function loadArticle(id) {
  displayBusyIndicator();
  window.location.href = 'view.php?articleId=' + id;
}

function resetBodyHeight() {
  $('footer').animate({ 'paddingBottom' : $('#mail-news-bar').outerHeight() });
}

function viewPost(id) {  
  displayLoadingToast();
  
  $.ajax({
      url: 'api.php',
      method: 'POST',
      data: {
          request: 'getPostFromId',
          content: [ id ]
      }
  })
  .done(function (data) {
      console.log(data);
      
      post = JSON.parse(data);
      
      if (post) {
          $('#postTitle').html(post['post_title']);
          $('#postContent').html(post['post_content']);
      }
      
      $('.toast').fadeOut();
      $('#postViewer').modal('show');
  })
  .fail(function (error) {
      console.error(error);
  });
}

$('document').ready(function () {
  let html = '';

  setProgress(25);

  $.ajax({
    url: 'api.php',
    type: 'POST',
    data: {
      request: 'getPinsForHome'
    }
  })
  .done(function (pins) {
    console.log(pins);
    
    try {
      pins = JSON.parse(pins);
    } catch (exception) {
      console.log(exception);

      doneLoading();
      $('main').append(`<div class="alert alert-danger container-lg" role="alert">
                          Não foi possível carregar os dados, tente novamente mais tarde ou <a href="" class="alert-link" onclick="displayBusyIndicator()">clique aqui</a> para tentar novamente.
                        </div>`);
      return;
    }
    let pinsHolder    = $('#pinsHolder');

    for (let i = 0; i < pins.length; i++) {
      let pin  = pins[i];

      let html = '<div class="card testimonial-card col-md-4">' +
                 ' <div class="avatar mx-auto white">' +
                 ' <img class="rounded-circle  img-thumbnail img-fixed-size" alt="woman avatar" src="' + (typeof(pin['pin_thumbnail']) != 'string' ? 'assets/images/logo.png' : TARGET_THUMBNAIL_PATH + pin['pin_thumbnail']) + '" />' +
                 ' </div>' +
                 ' <div class="card-body">' +
                 ' <h4 class="card-title">' + pin['pin_title'] + '</h4>' +
                 ' <hr>' +
                 ' <p>' + pin['pin_summary'] + '</p>' +
                 ' <p><a class="btn btn-secondary" href="' + pin['pin_target'] + '" role="button">Ler mais ...</a></p>' +
                 '</div><!-- /.col-lg-4 -->';
      pinsHolder.append(html);

      pinsHolder.fadeIn();
    }

    
    setProgress(50);

    // |====================================|
    // | BEGINNING OF SECOND SECTION LOADER |
    // |====================================|

    $.ajax({
      url: 'api.php',
      type: 'POST',
      data: {
        request: 'getPostsForHome'
      }
    })
    .done(function (posts) {
      console.log(posts);
  
      posts = JSON.parse(posts); html = '';
  
      posts.forEach(post => {
        console.log(post);
        html += '<div class="card-post col-md-4")">' +
                '  <div class="card mb-4 shadow-sm"><a href="' + (post['post_target'].includes('://') ? '' : 'nossas atividades/') + post['post_target'] + '?id=' + post['post_id'] + '">' +
                '    <img class="card-img-top img-fluid" alt="Responsive image" src="' + (typeof(post['post_thumbnail']) != 'string' ? 'assets/images/logo.png' : TARGET_THUMBNAIL_PATH + post['post_thumbnail']) + '" />' +
                '    <div class="overlay-text text-white">' + post['post_title'] + '</div>' +
                '  </a></div>' +
                '</div>';   
      });
  
      let postsHolder = $('#postsHolder');

      postsHolder.children('.row').append(html);
      postsHolder.fadeIn(ANIMATION_TIME);

      setProgress(100);
    })
    .fail(function (error) {
      console.error(error);
    });
  })
  .fail(function (error) {
  console.error(error);
  });
});

