$('document').ready(function() {
  $.ajax({
      url: 'api.php',
      type: 'POST',
      data: {
        request: 'getPinsForHome'
      }
    })
    .done(function (pins) {
      console.log(pins);
      
      try {
        pins = JSON.parse(pins);
      } catch (exception) {
        console.log(exception);

        doneLoading();
        $('main').append(`<div class="alert alert-danger container-lg" role="alert">
                            Não foi possível carregar os dados, tente novamente mais tarde ou <a href="" class="alert-link" onclick="displayBusyIndicator()">clique aqui</a> para tentar novamente.
                          </div>`);
        return;
      }
      let pinsHolder    = $('#pinsHolder');

      for (let i = 0; i < pins.length; i++) {
        let pin  = pins[i];

        let html = '<div class="card testimonial-card col-md-4">' +
                   ' <div class="avatar mx-auto white">' +
                   ' <img class="rounded-circle  img-thumbnail img-fixed-size" alt="woman avatar" src="' + (typeof(pin['pin_thumbnail']) != 'string' ? 'assets/images/logo.png' : TARGET_THUMBNAIL_PATH + pin['pin_thumbnail']) + '" />' +
                   ' </div>' +
                   ' <div class="card-body">' +
                   ' <h4 class="card-title">' + pin['pin_title'] + '</h4>' +
                   ' <hr>' +
                   ' <p>' + pin['pin_summary'] + '</p>' +
                   ' <p><a class="btn btn-secondary" href="' + pin['pin_target'] + '" role="button">Ler mais ...</a></p>' +
                   '</div><!-- /.col-lg-4 -->';
        pinsHolder.append(html);

        pinsHolder.fadeIn();
      }
    });  
});