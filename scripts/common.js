let waitingForCompletion = null;
let busyLoading = null;
let progressBar = $('#documentBusyProgress').children('.progress-bar');

$(".facebook").click(function(){window.open("https://www.facebook.com/CasadoConhecimentoSP", '_blank');});
$(".instagram").click(function(){window.open("https://www.facebook.com/CasadoConhecimentoSP", '_blank');});
$(".youtube").click(function(){window.open("https://www.youtube.com/watch?v=IU9HNZLhQ3o&feature=youtu.be", '_blank');});
$("#Asista-agora").click(function(){window.location = 'TV.php';});


function displayBusyIndicator() {
  $('#globalBusyIndicator').animate({ 'top' : '3%' }, ANIMATION_TIME * 2);
}

function setProgress(value) {
  let progressBar = $('#indexLoader .progress-bar');

  if (progressBar.attr('aria-valuenow') < 101) {
    progressBar.attr('aria-valuenow', value).width(value + '%');
  }
}

function doneLoading() {
  $('#indexLoader').fadeOut(ANIMATION_TIME);
  setTimeout(function () {
    $('#indexLoader').remove();
    resetBodyHeight();
  }, ANIMATION_TIME);
}

function displayLoadingToast() {
  $.toast({
    title: 'Carregando ...',
    type: 'info',
    delay: 5000
  });
}

function displaySuccessToast() {
  $.toast({
    title: 'Êxito!',
    type: 'success',
    delay: 2000
  });
}

$('document').ready(function() {
  $("#toast_show").toast({delay:2000});

  $('body').height($('body').height() + $('#mail-news-bar').height());

  console.log('Loading...');
  busyLoading = setInterval(function () { 
    if (progressBar.attr('aria-valuenow') < 101) {
      progressBar.attr('aria-valuenow', parseInt(progressBar.attr('aria-valuenow')) + 1).width((parseInt(progressBar.attr('aria-valuenow')) + 1) + '%');
    }
  }, 250); // Start showing some progress.


  $('#hideBottomBar').on('click', function () {
    // $('#mail-news-bar').outerHeight() * (-1) - grabs the outer height and makes it negative.
    $('#mail-news-bar').animate({ 'bottom' : ((($('#mail-news-bar').outerHeight() + $('#hideBottomBar').outerHeight()) * (-1))) });
    $('#showBottomBar').animate({ 'bottom' : '-1.5rem' });
    $('footer').animate({ 'paddingBottom' : 0 });
  });

  $('#showBottomBar').on('click', function () {
    $('#showBottomBar').animate({ 'bottom' : ($('#showBottomBar').outerHeight() * (-1)) });
    $('#mail-news-bar').animate({ 'bottom' : 0 });
    $('footer').animate({ 'paddingBottom' : $('#mail-news-bar').outerHeight() });
  });

  $('.image-input').on('change', function () {
    if ($(this)[0].files.length > 0) {
      if ($(this)[0].files[0].type.split('/').includes('image')) {
        $(this).removeClass('is-invalid');
        $('.save-button').attr('disabled', false);
      } else {
        $(this).addClass('is-invalid');
        $('.save-button').attr('disabled', true);

        $.toast({
          title: 'O arquivo deve ser uma imagem.',
          type: 'danger',
          delay: 5000
        });
      }
    }
  });
});

$(window).on('load', function () {
  if (busyLoading == null) {
    clearTimeout(waitingForCompletion);
  } else {
    console.log('Load complete!');

    setTimeout(function () {
      clearInterval(busyLoading);
    
      progressBar.attr('aria-valuenow', 100).width('100%');
      
      $('#documentBusyProgress').fadeOut();
    }, 500);
  }
});

//INFORMA QUE EL PROCEDIMIENTO O X ACTIVIDAD NO ESTA DISPONIBLE
function noDisponible(){
  let txt = 'Procedimiento no disponible';
  mostrar_toast(txt,'NO DISPONIBLE');
}


//VERIFICA SI EXISTE UN ERROR O SI TIENE PERMITIDO EL PASO
function permitido (data) {
  data = parseInt(data);
  let bandera = false;
  switch (data) {
    case 1:
      mostrar_toast('ERROR','ERROR');
    break;
    case 2:
      mostrar_toast('No tenés permitido el acceso','ERROR');
	    setTimeout(function() {
	        window.location = 'index.php';
	      }, 2000);
    break;
    default:
      bandera = true;
    break;
  }
  return bandera;
}


//EN EL CASO DE QUE OCURRA UN ERROR EN LA API
function ERRORFATAL(){
  let txt = 'Hubo un problema al conectarse con el servidor, ¿estás conectado a la red?';
  mostrar_toast(txt,'ERRORR FATAL');
}


//MUESTRA UN TOAST DURANTE 2 SEGUNDOS RECIBIENDO EL CONTENIDO(txt) Y UN MOTIVO PARA PONER EN EL HEADER(motivo)
function mostrar_toast(txt,motivo){
  $("#toast_body").html(txt);
  $("#toast_header").html(motivo);
  $("#toast_show").toast('show');
}

//FUNCIONES PARA IMAGENES LINKEABLES
  function redireccionar(link){
    window.location = link;
  }