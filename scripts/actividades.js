$('document').ready(function() {
  $.ajax({
      url: '../api.php',
      type: 'POST',
      data: {
        request: 'getPostsForHome'
      }
    })
    .done(function (posts) {
      console.log(posts);
  
      posts = JSON.parse(posts); html = '';
  
      posts.forEach(post => {
        console.log(post);
        html += '<div class="card-post col-md-4">' +
                '  <div class="card mb-4 shadow-sm"><a href="'+post['post_target']+'?id=' + post['post_id'] + '">' +
                '    <img class="card-img-top img-fluid" alt="Responsive image" src="../' + (typeof(post['post_thumbnail']) != 'string' ? 'assets/images/logo.png' : TARGET_THUMBNAIL_PATH + post['post_thumbnail']) + '" />' +
                '    <div class="overlay-text text-white actividad-text">' + post['post_title'] + '</div>' +
                '  </a></div>' +
                '</div>';  
      });
      let postsHolder = $('#postsHolder');

      postsHolder.children('.row').append(html);
      postsHolder.fadeIn(ANIMATION_TIME);


      
      setProgress(100);

      var page ="Mindfulness";

      $.ajax({
          url: '../api.php',
          type: 'POST',
          data: {
            request: 'getDescripcion',
            content: [page]
          }
          })
          .done(function (data) {
              if (permitido(data)) {
                $(".Descripcion").html(data);
                setProgress(100);
              };
          })
          .fail(function (data) {
            //SI LA API LLEGA A DAR UN ERROR FATAL
              ERRORFATAL()
      });


    })
    .fail(function (error) {
      console.error(error);
    });
});