$('document').ready(function () {
    $.ajax({
        url: 'api.php',
        method: 'POST',
        data: {
            request: 'getArticleFromId',
            content: [ $('#selectedArticle').val() ]
        }
    })
    .done(function (data) {
        console.log(data);

        article = JSON.parse(data);

        setProgress(50);
        $('header').css('background', 'url('+ TARGET_THUMBNAIL_PATH + article['article_thumbnail'] + ')');
        $('#articleTitle').html(article['article_title']);
        $('#articleContent').html(article['article_content']);
        setProgress(75);
        article['article_keywords'].split(' ').forEach(keyword => {
            $('#articleKeywords').append('<button type="button" class="btn btn-primary btn-sm lead my-3 mr-2">' + keyword + '</button>');
        });
        setProgress(100);
        doneLoading();
    })
    .fail(function (error) {
        console.error(error);
    });
});