function lookForChanges() {
    if ($('#postCreationModal').is(':visible')) {
        if ($('#postTitle').val().length > 0 || $('#postContent').val().length > 0) {
            $('#warnLostDataModal').modal('show');
        } else {
            $('#postCreationModal').modal('hide');
        }
    } else {
        if ($('#postModTitle').val().length > 0 || $('#postModContent').val().length > 0) {
            $('#warnLostDataModal').modal('show');
        } else {
            $('#postCreationModal').modal('hide');
        }
    }
}

function addPost() {
    $('#postCreationModal').modal('show');
}

function editPost(id) {
    $('#postModId').val(id);
    
    displayLoadingToast();
    
    $.ajax({
        url: 'api.php',
        method: 'POST',
        data: {
            request: 'getPostFromId',
            content: [ id ]
        }
    })
    .done(function (data) {
        console.log(data);
        
        post = JSON.parse(data);
        
        if (post) {
            $('#postModTitle').val(post['post_title']);
            $('#postModAuthor').val(post['post_author']);
            $('#postModThumbnail').val('');


            $('#postContent, #postModContent').trumbowyg();
            $('.trumbowyg-editor')[1].innerHTML = post['post_content'];
            $('#postModContent').val(post['post_content']);
            $('#postModTarget').val(post['post_target']);
        }
        
        $('.toast').fadeOut();
        $('#postModificationModal').modal('show');
    })
    .fail(function (error) {
        console.error(error);
    });
}

function delPost(id) {
    $('#toDeleteId').val(id);
    
    $('#deleteConfirmationModal').modal('show');
}

$('document').ready(function () {
    var postContentEditor = $('#postContent, #postModContent');

    $.ajax({
        url: 'api.php',
        method: 'POST',
        data: {
            request: 'getPostsForManager'
        }
    })
    .done(function (data) {
        console.log(data);
        
        data = JSON.parse(data);
        
        let html =  '<table id="postDataHolder" class="table" style="display: none;">' +
                    '  <thead>' +
                    '    <tr>' +
                    '      <th scope="col">Título</th>' +
                    '      <th scope="col">Data e hora</th>' +
                    '      <th scope="col">Autor</th>' +
                    '      <th scope="col">Opções</th>' +
                    '    </tr>' +
                    '  </thead>';
        
        data.forEach(post => {
            console.log(post);
            
            html += tr(td(post['post_title']) +
            td(post['post_datetime']) +
            td(post['post_author']) +
            td('<button type="button" class="btn btn-warning mr-3" onclick="editPost(' + post['post_id'] + ')"><i class="fa fa-pencil"></i></button>' + 
            '<button type="button" class="btn btn-danger"       onclick="delPost (' + post['post_id'] + ')"><i class="fa fa-trash"></i></button>'));
        });
        
        html += '</table>';
        
        $('#postsLoader').after(html).fadeOut(ANIMATION_TIME);
        
        setTimeout(function () {
            $('#postsLoader').remove();
            
            setTimeout(function () {
                $('#addPost, #postDataHolder').fadeIn();
            }, ANIMATION_TIME);
        }, ANIMATION_TIME);
    })
    .fail(function (error) {
        console.log(error);
        
        $('#postsLoader').fadeOut(ANIMATION_TIME);
        setTimeout(function () {
            $('#postsLoader')
            .after('<div class="alert alert-danger" role="alert" style="display: none">' +
            'Não foi possível carregar as postagens.' +
            '</div>')
            .remove();
            
            $('.alert').fadeIn();
        }, ANIMATION_TIME);
        
        $.toast({
            title: 'Não foi possível carregar as postagens.',
            type: 'danger',
            delay: 2000
        });
    });

    if (postContentEditor.length > 0) {
        let normalLabel = $('#fileBox').html();
        postContentEditor.trumbowyg();
        
        // We don't want the user to see the data changing in real-time,
        // lets do it right once the modal's got visually unreachable.
        $('#postCreationModal').on('hidden.bs.modal', function () {
            $('#postTitle, #postContent').val('');
            $('#fileBox label').html(normalLabel);
        });
        
        $('#postCreationModal, #postModificationModal').on('hidePrevented.bs.modal', function () {
            lookForChanges();
        });
        
        $('.closePostCreationModal').on('click', function () {
            lookForChanges();
        });
        
        $('#acceptDataLost').on('click', function () {
            $('#warnLostDataModal, #postCreationModal, #postModificationModal').modal('hide');
        });
        
        $('#fileBox input').on('change', function () {
            lookForFiles($(this));
        });
        
        $('#acceptPostDeletion').on('click', function () {
            displayLoadingToast();
            
            $.ajax({
                url: 'api.php',
                type: 'POST',
                data: {
                    request: 'delPost',
                    content: [ $('#toDeleteId').val() ]
                }
            })
            .done(function (data) {
                console.log(data);
                
                $('#deleteConfirmationModal').modal('hide');
                $('.toast').fadeOut();
                
                if (data == PASS) {
                    displaySuccessToast();
                } else {
                    $.toast({
                        title: 'Ocorreu um problema ao excluir a postagem.',
                        type: 'danger',
                        delay: 2000
                    });
                }
            })
            .fail(function (error) {
                console.log(error);
                
                $.toast({
                    title: 'Ocorreu um problema ao excluir a postagem.',
                    type: 'danger',
                    delay: 2000
                });
            });
        });
        
        $('#savePost').on('click', function () {
            $.toast({
                title: 'Fazendo publicação ...',
                type: 'info',
                delay: 2000
            });
            
            let formData = new FormData(); let position = -1;
            
            // This functions prevents hardcoding 
            // the array positions.
            function getNewPosition() {
                position++;
                return position;
            }
            
            formData.append('request', 'addPost');
            formData.append('content[' + getNewPosition() + ']', $('#postTitle').val());
            formData.append('content[' + getNewPosition() + ']', $('#postAuthor').val());
            formData.append('content[' + getNewPosition() + ']', $('#postContent').val());
            formData.append('content[' + getNewPosition() + ']', $('#postTarget').val());
            
            if ($('#postThumbnail')[0].files.length > 0) {
                formData.append('thumbnail', $('#postThumbnail')[0].files[0], 'thumb.jpg');
            }
            
            $.ajax({
                url: 'api.php',
                method: 'POST',
                data: formData,
                contentType: false,
                processData: false
            })
            .done(function (data) {
                $('.toast').fadeOut();
                console.log(data);
                
                data = parseInt(data);
                
                if (data == PASS) {
                    $('#postCreationModal').modal('hide');
                    $('#postTitle, #postContent, #postThumbnail, #postTarget').val('');
                    
                    displaySuccessToast();
                } else if (data == NOT_AN_IMAGE) {
                    $.toast({
                        title: 'O arquivo enviado não é uma imagem.',
                        type: 'danger',
                        delay: 2000
                    });
                } else {
                    $.toast({
                        title: 'Falha ao postar, tente novamente.',
                        type: 'danger',
                        delay: 2000
                    });
                }
            })
            .fail(function (error) {
                console.error(error.responseText);
            });
        });
        
        
        $('#saveModPost').on('click', function () {
            const NO_CHANGES = 4;
            
            $.toast({
                title: 'Salvando alterações ...',
                type: 'info',
                delay: 2000
            });
            
            let formData = new FormData(); let position = -1;
            
            // This functions prevents hardcoding 
            // the array positions.
            function getNewPosition() {
                position++;
                return position;
            }
            
            formData.append('request', 'modPost');
            formData.append('content[' + getNewPosition() + ']', $('#postModId').val());
            formData.append('content[' + getNewPosition() + ']', $('#postModTitle').val());
            formData.append('content[' + getNewPosition() + ']', $('#postModContent').val());
            formData.append('content[' + getNewPosition() + ']', $('#postModTarget').val());
            
            if ($('#postModThumbnail')[0].files.length > 0) {
                formData.append('thumbnail', $('#postModThumbnail')[0].files[0], 'thumb.jpg');
            }
            
            $.ajax({
                url: 'api.php',
                method: 'POST',
                data: formData,
                contentType: false,
                processData: false
            })
            .done(function (data) {
                $('.toast').fadeOut();
                console.log(data);
                
                data = parseInt(data);
                
                switch (data) {
                    case PASS:
                        $('#postModificationModal').modal('hide');
                        $('#postModTitle, #postModContent, #postModTarget').val('');
                        
                        displaySuccessToast();

                        break;
                    case NO_CHANGES:
                        $('#postModificationModal').modal('hide');
                        $('#postModTitle, #postModContent').val('');
                        
                        $.toast({
                            title: 'Nenhuma alteração foi feita.',
                            type: 'warning',
                            delay: 2000
                        });

                        break;
                    case NOT_AN_IMAGE:
                        $.toast({
                            title: 'O arquivo enviado não é uma imagem.',
                            type: 'danger',
                            delay: 2000
                        });
                        
                        break;
                    default:
                        $.toast({
                            title: 'Falha ao salvar, tente novamente.',
                            type: 'danger',
                            delay: 2000
                        });
                }
            })
            .fail(function (error) {
                console.error(error.responseText);
            });
        });
    }
});