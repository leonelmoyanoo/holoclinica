function lookForChanges() {
    if ($('#pinCreationModal').is(':visible')) {
        if ($('#pinTitle').val().length > 0 || $('#pinSummary').val().length > 0) {
            $('#warnLostDataModal').modal('show');
        } else {
            $('#pinCreationModal').modal('hide');
        }
    } else {
        if ($('#pinModTitle').val().length > 0 || $('#pinModContent').val().length > 0) {
            $('#warnLostDataModal').modal('show');
        } else {
            $('#pinCreationModal').modal('hide');
        }
    }
}

function addPin() {
    $('#pinCreationModal').modal('show');
}

function editPin(id) {
    $('#pinModId').val(id);
    
    displayLoadingToast();
    
    $.ajax({
        url: 'api.php',
        method: 'POST',
        data: {
            request: 'getPinFromId',
            content: [ id ]
        }
    })
    .done(function (data) {
        console.log(data);
        
        pin = JSON.parse(data);
        
        if (pin) {
            $('#pinModTitle').val(pin['pin_title']);
            $('#pinModTarget').val(pin['pin_target']);
            
            $('#pinSummary, #pinModSummary').trumbowyg();
            $('.trumbowyg-editor')[1].innerHTML = pin['pin_summary'];
            $('#pinModSummary').val(pin['pin_summary']);
            $('#pinModThumbnail').val('');
            $('.custom-file-label').html(FILE_SELECT_LABEL_TEXT);
        }
        
        $('.toast').fadeOut();
        $('#pinModificationModal').modal('show');
    })
    .fail(function (error) {
        console.error(error);
    });
}

function delPin(id) {
    $('#toDeleteId').val(id);
    
    $('#deleteConfirmationModal').modal('show');
}

$('document').ready(function () {
    var postContentEditor = $('#pinSummary, #pinModContent');

    $.ajax({
        url: 'api.php',
        method: 'POST',
        data: {
            request: 'getPinsForManager'
        }
    })
    .done(function (data) {
        console.log(data);
        
        data = JSON.parse(data);

        if (data.length > 0) {
            
            let html =  '<table id="pinDataHolder" class="table" style="display: none;">' +
                        '  <thead>' +
                        '    <tr>' +
                        '      <th scope="col">Título</th>' +
                        '      <th scope="col">Destino</th>' +
                        '      <th scope="col">Sumário</th>' +
                        '      <th scope="col">Opções</th>' +
                        '    </tr>' +
                        '  </thead>';
            
            data.forEach(pin => {
                console.log(pin);
                
                html += tr(td(pin['pin_title']) +
                td('<a href="' + pin['pin_target'] + '">' + pin['pin_target']  + '</a>') +
                td(pin['pin_summary']) +
                td('<button type="button" class="btn btn-warning mr-3" onclick="editPin(' + pin['pin_id'] + ')"><i class="fa fa-pencil"></i></button>' + 
                   '<button type="button" class="btn btn-danger"       onclick="delPin (' + pin['pin_id'] + ')"><i class="fa fa-trash"></i></button>'));
            });
            
            html += '</table>';
            
            $('#pinsLoader').after(html).fadeOut(ANIMATION_TIME);
        } else {
            $('#pinsLoader').after('<div class="alert alert-light" role="alert" style="display: none">' +
                                   '    Nada por aqui.' +
                                   '</div>');
        }
            
        setTimeout(function () {
            $('#pinsLoader').remove();
            
            setTimeout(function () {
                $('#addPin, #pinDataHolder, .alert').fadeIn();
            }, ANIMATION_TIME);
        }, ANIMATION_TIME);
    })
    .fail(function (error) {
        console.log(error);
        
        $('#pinsLoader').fadeOut(ANIMATION_TIME);
        setTimeout(function () {
            $('#pinsLoader')
            .after('<div class="alert alert-danger" role="alert" style="display: none">' +
            'Não foi possível carregar as postagens.' +
            '</div>')
            .remove();
            
            $('.alert').fadeIn();
        }, ANIMATION_TIME);
        
        $.toast({
            title: 'Não foi possível carregar as postagens.',
            type: 'danger',
            delay: 2000
        });
    });

    if (postContentEditor.length > 0) {
        let normalLabel = $('#fileBox').html();
        postContentEditor.trumbowyg();
        
        // We don't want the user to see the data changing in real-time,
        // lets do it right once the modal's got visually unreachable.
        $('#pinCreationModal').on('hidden.bs.modal', function () {
            $('#pinTitle, #pinSummary').val('');
            $('#fileBox label').html(normalLabel);
        });
        
        $('#pinCreationModal, #pinModificationModal').on('hidePrevented.bs.modal', function () {
            lookForChanges();
        });
        
        $('.closePinCreationModal').on('click', function () {
            lookForChanges();
        });
        
        $('#acceptDataLost').on('click', function () {
            $('#warnLostDataModal, #pinCreationModal, #pinModificationModal').modal('hide');
        });
        
        $('#pinThumbnail').on('change', function () {
            lookForFiles($('#pinModThumbnail'))
        });

        $('#pinModThumbnail').on('change', function () {
            lookForFiles($('#pinModThumbnail'));
        });
        
        $('#acceptPinDeletion').on('click', function () {
            displayLoadingToast();
            
            $.ajax({
                url: 'api.php',
                type: 'POST',
                data: {
                    request: 'delPin',
                    content: [ $('#toDeleteId').val() ]
                }
            })
            .done(function (data) {
                console.log(data);
                
                $('#deleteConfirmationModal').modal('hide');
                $('.toast').fadeOut();
                
                if (data == PASS) {
                    displaySuccessToast();
                } else {
                    $.toast({
                        title: 'Ocorreu um problema ao excluir a postagem.',
                        type: 'danger',
                        delay: 2000
                    });
                }
            })
            .fail(function (error) {
                console.log(error);
                
                $.toast({
                    title: 'Ocorreu um problema ao excluir a postagem.',
                    type: 'danger',
                    delay: 2000
                });
            });
        });
        
        $('#savePin').on('click', function () {
            $.toast({
                title: 'Fazendo publicação ...',
                type: 'info',
                delay: 2000
            });
            
            let formData = new FormData(); let position = -1;
            
            // This functions prevents hardcoding 
            // the array positions.
            function getNewPosition() {
                position++;
                return position;
            }
            
            formData.append('request', 'addPin');
            formData.append('content[' + getNewPosition() + ']', $('#pinTitle').val());
            formData.append('content[' + getNewPosition() + ']', $('#pinTarget').val());
            formData.append('content[' + getNewPosition() + ']', $('#pinSummary').val());
            
            if ($('#pinThumbnail')[0].files.length > 0) {
                formData.append('thumbnail', $('#pinThumbnail')[0].files[0], 'thumb.jpg');
            }
            
            $.ajax({
                url: 'api.php',
                method: 'POST',
                data: formData,
                contentType: false,
                processData: false
            })
            .done(function (data) {
                $('.toast').fadeOut();
                console.log(data);
                
                data = parseInt(data);
                
                if (data == PASS) {
                    $('#pinCreationModal').modal('hide');
                    $('#pinTitle, #pinSummary, #pinThumbnail').val('');
                    
                    displaySuccessToast();
                } else if (data == NOT_AN_IMAGE) {
                    $.toast({
                        title: 'O arquivo enviado não é uma imagem.',
                        type: 'danger',
                        delay: 2000
                    });
                } else {
                    $.toast({
                        title: 'Falha ao postar, tente novamente.',
                        type: 'danger',
                        delay: 2000
                    });
                }
            })
            .fail(function (error) {
                console.error(error.responseText);
            });
        });
        
        
        $('#saveModPin').on('click', function () {
            const NO_CHANGES = 4;
            
            $.toast({
                title: 'Salvando alterações ...',
                type: 'info',
                delay: 2000
            });
            
            let formData = new FormData(); let position = -1;
            
            // This functions prevents hardcoding 
            // the array positions.
            function getNewPosition() {
                position++;
                return position;
            }
            
            formData.append('request', 'modPin');
            formData.append('content[' + getNewPosition() + ']', $('#pinModId').val());
            formData.append('content[' + getNewPosition() + ']', $('#pinModTitle').val());
            formData.append('content[' + getNewPosition() + ']', $('#pinModSummary').val());
            formData.append('content[' + getNewPosition() + ']', $('#pinModTarget').val());
            
            if ($('#pinModThumbnail')[0].files.length > 0) {
                formData.append('thumbnail', $('#pinModThumbnail')[0].files[0], 'thumb.jpg');
            }
            
            $.ajax({
                url: 'api.php',
                method: 'POST',
                data: formData,
                contentType: false,
                processData: false
            })
            .done(function (data) {
                $('.toast').fadeOut();
                console.log(data);
                
                data = parseInt(data);
                
                switch (data) {
                    case PASS:
                        $('#pinModificationModal').modal('hide');
                        $('#pinModTitle, #pinModContent').val('');
                        
                        displaySuccessToast();

                        break;
                    case NO_CHANGES:
                        $('#pinModificationModal').modal('hide');
                        $('#pinModTitle, #pinModContent').val('');
                        
                        $.toast({
                            title: 'Nenhuma alteração foi feita.',
                            type: 'warning',
                            delay: 2000
                        });

                        break;
                    case NOT_AN_IMAGE:
                        $.toast({
                            title: 'O arquivo enviado não é uma imagem.',
                            type: 'danger',
                            delay: 2000
                        });
                        
                        break;
                    default:
                        $.toast({
                            title: 'Falha ao salvar, tente novamente.',
                            type: 'danger',
                            delay: 2000
                        });
                }
            })
            .fail(function (error) {
                console.error(error.responseText);
            });
        });
    }
});