const PASS = 0;
const ERROR = -1;
const NOT_ALLOWED = -2;
const NOT_AN_IMAGE = -3;

const FILE_SELECT_LABEL_TEXT = 'Selecionar arquivo';

function tr(data) {
  return '<tr>' + data + '</tr>';
}

function td(data) {
  return '<td>' + data + '</td>';
}

function lookForFiles(fileHolder) {
  if (fileHolder[0].files.length > 0) {
      fileHolder.next().html(fileHolder[0].files[0]['name']);
  } else {
      fileHolder.next().html(FILE_SELECT_LABEL_TEXT);
  }
}