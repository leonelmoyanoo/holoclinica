function lookForChanges() {
    if ($('#articleCreationModal').is(':visible')) {
        if ($('#articleTitle').val().length > 0 || $('#articleContent').val().length > 0) {
            $('#warnLostDataModal').modal('show');
        } else {
            $('#articleCreationModal').modal('hide');
        }
    } else {
        if ($('#articleModTitle').val().length > 0 || $('#articleModContent').val().length > 0) {
            $('#warnLostDataModal').modal('show');
        } else {
            $('#articleCreationModal').modal('hide');
        }
    }
}

function addArticle() {
    $('#articleCreationModal').modal('show');
}

function editArticle(id) {
    $('#articleModId').val(id);
    
    displayLoadingToast();
    
    $.ajax({
        url: 'api.php',
        method: 'POST',
        data: {
            request: 'getArticleFromId',
            content: [ id ]
        }
    })
    .done(function (data) {
        console.log(data);
        
        article = JSON.parse(data);
        
        if (article) {
            $('#articleModTitle').val(article['article_title']);
            $('#articleModAuthor').val(article['article_author']);
            $('#articleModKeywords').val(article['article_keywords']);

            $('#articleContent, #articleModContent').trumbowyg();
            $('.trumbowyg-editor')[1].innerHTML = article['article_content'];
            $('#articleModContent').val(article['article_content']);
        }
        
        $('.toast').fadeOut();
        $('#articleModificationModal').modal('show');
    })
    .fail(function (error) {
        console.error(error);
    });
}

function delArticle(id) {
    $('#toDeleteId').val(id);
    
    $('#deleteConfirmationModal').modal('show');
}

$('document').ready(function () {
    var articleContentEditor = $('#articleContent, #articleModContent');

    $.ajax({
        url: 'api.php',
        method: 'POST',
        data: {
            request: 'getArticlesForManager'
        }
    })
    .done(function (data) {
        console.log(data);
        
        data = JSON.parse(data);
        
        let html =  '<table id="articleDataHolder" class="table" style="display: none;">' +
                    '  <thead>' +
                    '    <tr>' +
                    '      <th scope="col">Título</th>' +
                    '      <th scope="col">Data e hora</th>' +
                    '      <th scope="col">Autor</th>' +
                    '      <th scope="col">Opções</th>' +
                    '    </tr>' +
                    '  </thead>';
        
        data.forEach(article => {
            console.log(article);
            
            html += tr(td(article['article_title']) +
            td(article['article_datetime']) +
            td(article['article_author']) +
            td('<button type="button" class="btn btn-warning mr-3" onclick="editArticle(' + article['article_id'] + ')"><i class="fa fa-pencil"></i></button>' + 
               '<button type="button" class="btn btn-danger"       onclick="delArticle (' + article['article_id'] + ')"><i class="fa fa-trash"></i></button>'));
        });
        
        html += '</table>';
        
        $('#articlesLoader').after(html).fadeOut(ANIMATION_TIME);
        
        setTimeout(function () {
            $('#articlesLoader').remove();
            
            setTimeout(function () {
                $('#addArticle, #articleDataHolder').fadeIn();
            }, ANIMATION_TIME);
        }, ANIMATION_TIME);
    })
    .fail(function (error) {
        console.log(error);
        
        $('#articlesLoader').fadeOut(ANIMATION_TIME);
        setTimeout(function () {
            $('#articlesLoader')
            .after('<div class="alert alert-danger" role="alert" style="display: none">' +
            'Não foi possível carregar as articleagens.' +
            '</div>')
            .remove();
            
            $('.alert').fadeIn();
        }, ANIMATION_TIME);
        
        $.toast({
            title: 'Não foi possível carregar as articleagens.',
            type: 'danger',
            delay: 2000
        });
    });

    if (articleContentEditor.length > 0) {
        let normalLabel = $('#fileBox').html();
        articleContentEditor.trumbowyg();
        
        // We don't want the user to see the data changing in real-time,
        // lets do it right once the modal's got visually unreachable.
        $('#articleCreationModal').on('hidden.bs.modal', function () {
            $('#articleTitle, #articleContent').val('');
            $('#fileBox label').html(normalLabel);
        });
        
        $('#articleCreationModal, #articleModificationModal').on('hidePrevented.bs.modal', function () {
            lookForChanges();
        });
        
        $('.closeArticleCreationModal').on('click', function () {
            lookForChanges();
        });
        
        $('#acceptDataLost').on('click', function () {
            $('#warnLostDataModal, #articleCreationModal, #articleModificationModal').modal('hide');
        });
           
        $('#articleThumbnail').on('change', function () {
            lookForFiles($('#articleThumbnail'));
        });

        $('#articleModThumbnail').on('change', function () {
            lookForFiles($('#articleModThumbnail'));
        });
        
        $('#acceptArticleDeletion').on('click', function () {
            displayLoadingToast();
            
            $.ajax({
                url: 'api.php',
                type: 'POST',
                data: {
                    request: 'delArticle',
                    content: [ $('#toDeleteId').val() ]
                }
            })
            .done(function (data) {
                console.log(data);
                
                $('#deleteConfirmationModal').modal('hide');
                $('.toast').fadeOut();
                
                if (data == PASS) {
                    displaySuccessToast();
                } else {
                    $.toast({
                        title: 'Ocorreu um problema ao excluir a articleagem.',
                        type: 'danger',
                        delay: 2000
                    });
                }
            })
            .fail(function (error) {
                console.log(error);
                
                $.toast({
                    title: 'Ocorreu um problema ao excluir a articleagem.',
                    type: 'danger',
                    delay: 2000
                });
            });
        });
        
        $('#saveArticle').on('click', function () {
            $.toast({
                title: 'Fazendo publicação ...',
                type: 'info',
                delay: 2000
            });
            
            let formData = new FormData(); let position = -1;
            
            // This functions prevents hardcoding 
            // the array positions.
            function getNewPosition() {
                position++;
                return position;
            }
            
            formData.append('request', 'addArticle');
            formData.append('content[' + getNewPosition() + ']', $('#articleTitle').val());
            formData.append('content[' + getNewPosition() + ']', $('#articleAuthor').val());
            formData.append('content[' + getNewPosition() + ']', $('#articleContent').val());
            formData.append('content[' + getNewPosition() + ']', $('#articleKeywords').val());
            
            if ($('#articleThumbnail')[0].files.length > 0) {
                formData.append('thumbnail', $('#articleThumbnail')[0].files[0], 'thumb.jpg');
            }
            
            $.ajax({
                url: 'api.php',
                method: 'POST',
                data: formData,
                contentType: false,
                processData: false
            })
            .done(function (data) {
                $('.toast').fadeOut();
                console.log(data);
                
                data = parseInt(data);
                
                if (data == PASS) {
                    $('#articleCreationModal').modal('hide');
                    $('#articleTitle, #articleContent, #articleThumbnail').val('');
                    
                    displaySuccessToast();
                } else if (data == NOT_AN_IMAGE) {
                    $.toast({
                        title: 'O arquivo enviado não é uma imagem.',
                        type: 'danger',
                        delay: 2000
                    });
                } else {
                    $.toast({
                        title: 'Falha ao articlear, tente novamente.',
                        type: 'danger',
                        delay: 2000
                    });
                }
            })
            .fail(function (error) {
                console.error(error.responseText);
            });
        });
        
        
        $('#saveModArticle').on('click', function () {
            const NO_CHANGES = 4;
            
            $.toast({
                title: 'Salvando alterações ...',
                type: 'info',
                delay: 2000
            });
            
            let formData = new FormData(); let position = -1;
            
            // This functions prevents hardcoding 
            // the array positions.
            function getNewPosition() {
                position++;
                return position;
            }
            
            formData.append('request', 'modArticle');
            formData.append('content[' + getNewPosition() + ']', $('#articleModId').val());
            formData.append('content[' + getNewPosition() + ']', $('#articleModTitle').val());
            formData.append('content[' + getNewPosition() + ']', $('#articleModContent').val());
            formData.append('content[' + getNewPosition() + ']', $('#articleModKeywords').val());
            
            if ($('#articleModThumbnail')[0].files.length > 0) {
                formData.append('thumbnail', $('#articleModThumbnail')[0].files[0], 'thumb.jpg');
            }
            
            $.ajax({
                url: 'api.php',
                method: 'POST',
                data: formData,
                contentType: false,
                processData: false
            })
            .done(function (data) {
                $('.toast').fadeOut();
                console.log(data);
                
                data = parseInt(data);
                
                switch (data) {
                    case PASS:
                        $('#articleModificationModal').modal('hide');
                        $('#articleModTitle, #articleModContent').val('');
                        
                        displaySuccessToast();

                        break;
                    case NO_CHANGES:    
                        $('#articleModificationModal').modal('hide');
                        $('#articleModTitle, #articleModContent').val('');
                        
                        $.toast({
                            title: 'Nenhuma alteração foi feita.',
                            type: 'warning',
                            delay: 2000
                        });

                        break;
                    case NOT_AN_IMAGE:
                        $.toast({
                            title: 'O arquivo enviado não é uma imagem.',
                            type: 'danger',
                            delay: 2000
                        });
                        
                        break;
                    default:    
                        $.toast({
                            title: 'Falha ao salvar, tente novamente.',
                            type: 'danger',
                            delay: 2000
                        });
                }
            })
            .fail(function (error) {
                console.error(error.responseText);
            });
        });
    }
});